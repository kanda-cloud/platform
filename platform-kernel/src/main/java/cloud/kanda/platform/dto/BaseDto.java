package cloud.kanda.platform.dto;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder; 
 
@Schema(name = "BaseDto", description = "Identification and audit fields")
@FieldNameConstants
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseDto {
	
	@Schema(name = "id", description = "Entity identifier")
	private String id; 
 
	@Schema(name = "createdBy", description = "User identifier of the creation")
    private String createdBy;

	@Schema(name = "lastModifiedBy", description = "User identifier of the last modification")
    private String lastModifiedBy;
    
	@Schema(name = "createdDate", description = "Created date")
    private Date createdDate; 
	
	@Schema(name = "lastModifiedDate", description = "Last modified date")
    private Date lastModifiedDate; 
    
	@Schema(name = "version", description = "Number of modifications")
    private Long version;
 
}
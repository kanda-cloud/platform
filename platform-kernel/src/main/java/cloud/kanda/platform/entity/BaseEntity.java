package cloud.kanda.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;

@FieldNameConstants
@SuperBuilder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseEntity implements EntityDocument<String> {

    public static final String INTERNAL_ID = "_id";

    @MongoId(FieldType.OBJECT_ID)
    private String id;

    @CreatedBy
    @Field("CREATED_BY")
    private String createdBy;

    @LastModifiedBy
    @Field("LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @CreatedDate
    @Field("CREATED_DATE")
    private Date createdDate;

    @LastModifiedDate
    @Field("LAST_MODIFIED_DATE")
    private Date lastModifiedDate;

    @Version
    @Field("VERSION")
    private Long version;

}
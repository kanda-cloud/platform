package cloud.kanda.platform.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
public interface EntityDocument<I extends Serializable> {

    I getId();

}

package cloud.kanda.platform.repository.imp;

import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import cloud.kanda.platform.utils.data.UtilsCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;

@RequiredArgsConstructor
public abstract class RepositoryCustomImpl {

    private final MongoTemplate mongoTemplate;

    public DataContext count(final DataContext dataContext) {
        dataContext.setResult(this.mongoTemplate.count(UtilsCriteria.getQuery(dataContext), dataContext.getObjectClass()));

        return dataContext;
    }

    public DataContext exists(final DataContext dataContext) {
        dataContext.setResult(this.mongoTemplate.exists(UtilsCriteria.getQuery(dataContext), dataContext.getObjectClass()));

        return dataContext;
    }

    public DataContextPageable findAll(final DataContextPageable dataContext) {
//		if (dataContext.getPageable() != null) {
//			dataContext.getPageable().setTotalElements((Long) count(dataContext).getResult());
//			dataContext.getPageable().setTotalPages(dataContext.getPageable().getTotalElements() / dataContext.getPageable().getSize());
//			if (dataContext.getPageable().getTotalElements() == 0) {
//				dataContext.setResult(Collections.emptyList());
//				return dataContext;
//			}
//		}
        dataContext.setResult(this.mongoTemplate.find(UtilsCriteria.getQuery(dataContext), dataContext.getObjectClass()));

        return dataContext;
    }

    public DataContext findOne(final DataContext dataContext) {
        dataContext.setResult(this.mongoTemplate.findOne(UtilsCriteria.getQuery(dataContext), dataContext.getObjectClass()));

        return dataContext;
    }

}

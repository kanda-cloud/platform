package cloud.kanda.platform.repository;

import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;

public interface RepositoryCustom {

	DataContext count(DataContext dataContext);
	
	DataContext exists(DataContext dataContext);
	
	DataContextPageable findAll(DataContextPageable dataContext);
	
	DataContext findOne(DataContext dataContext);

}

package cloud.kanda.platform.repository.imp;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CriteriaOperatorEnum {

	@SerializedName(value="AND", alternate="and")
	AND("and"),
	@SerializedName(value="OR", alternate="or")
	OR("or"),
	
	@SerializedName(value="EQUALS", alternate="eq")
	EQUALS("eq"),
	@SerializedName(value="NOT_EQUALS", alternate="ne")
	NOT_EQUALS("ne"),
	@SerializedName(value="LIKE", alternate="lk")
    LIKE("lk"),
    @SerializedName(value="START_WITH", alternate="st")
    START_WITH("st"),
    @SerializedName(value="END_WITH", alternate="en")
    END_WITH("en"),
    @SerializedName(value="LESS_THAN", alternate="ls")
    LESS_THAN("ls"),
    @SerializedName(value="GREATHER_THAN", alternate="gt")
    GREATHER_THAN("gt"),
    @SerializedName(value="LESS_EQUALS_THAN", alternate="le")
    LESS_EQUALS_THAN("le"),
    @SerializedName(value="GREATHER_EQUALS_THAN", alternate="ge")
    GREATHER_EQUALS_THAN("ge"),
    @SerializedName(value="COMBON_AND", alternate="ca")
    COMBON_AND("ca"),
    @SerializedName(value="COMBO_OR", alternate="co")
    COMBO_OR("co"),
    @SerializedName(value="NULLABLE", alternate="nl")
    NULLABLE("nl"),
    @SerializedName(value="EMPTY", alternate="em")
    EMPTY("em"),
    @SerializedName(value="EXCLUDE", alternate="ex")
    EXCLUDE("ex");
		    
	private final String key;
	
	public static CriteriaOperatorEnum getCriteriaName(final String key) {
		if (key != null) {
			for (CriteriaOperatorEnum criteria : CriteriaOperatorEnum.values()) {
				if (key.equals(criteria.getKey())) {
	                return criteria;
	            }
	        }
		}
        
        return null;
	}
	
}

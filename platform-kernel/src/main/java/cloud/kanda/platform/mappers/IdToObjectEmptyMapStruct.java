package cloud.kanda.platform.mappers;

import org.mapstruct.TargetType;

import cloud.kanda.platform.entity.BaseEntity;
import lombok.experimental.UtilityClass;

@UtilityClass
public class IdToObjectEmptyMapStruct {

    public <T extends BaseEntity> T asEntity(final String id,
            @TargetType final Class<T> entityClass) throws ReflectiveOperationException {
        if (id == null) {
            return null;
        }

        final T target = entityClass.getConstructor(entityClass).newInstance();
        target.setId(id);

        return target;
    }

    public <T extends BaseEntity> String fromEntityToId(final T target) {
        return target != null ? target.getId() : null;
    }
    
}

package cloud.kanda.platform.mappers;

import cloud.kanda.platform.utils.data.DataContext;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.MapperConfig;
import org.mapstruct.MappingInheritanceStrategy;

import java.util.Collection;

@MapperConfig(mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG)
public interface BaseMapStruct<D, E> {

    DataContext convertToEntityDataContext(DataContext dataContext);

    @InheritInverseConfiguration(name = "convertToEntityDataContext")
    DataContext convertToDtoDataContext(DataContext entity);

    E convertToEntity(D resource);

    Collection<E> convertToEntities(Collection<D> collection);

    @InheritInverseConfiguration(name = "convertToEntity")
    D convertToDto(E entity);

    Collection<D> convertToDtos(Collection<E> collection);

}

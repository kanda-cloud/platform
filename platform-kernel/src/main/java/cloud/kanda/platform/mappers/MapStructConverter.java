package cloud.kanda.platform.mappers;

import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.utils.data.DataContext;
import lombok.experimental.UtilityClass;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.List;

@SuppressWarnings("unchecked")
@UtilityClass
public class MapStructConverter {

    public <D, E, M extends BaseMapStruct<D, E>> D convertToDto(final E entity, final Class<M> mapperClass) {
        return Mappers.getMapper(mapperClass).convertToDto(entity);
    }

    public <E, D, M extends BaseMapStruct<D, E>> E convertToEntity(final D dto, final Class<M> mapperClass) {
        return Mappers.getMapper(mapperClass).convertToEntity(dto);
    }

    public <E, D, M extends BaseMapStruct<D, E>> DataContext convertToDto(final DataContext dataContext, final Class<M> mapperClass) {
        final M mapper = Mappers.getMapper(mapperClass);
        if (dataContext.getObject() != null) {
            if (dataContext.getObject() instanceof Collection<?>) {
                dataContext.setObject(mapper.convertToDtos((List<E>) dataContext.getObject()));
            } else {
                dataContext.setObject(mapper.convertToDto((E) dataContext.getObject()));
            }
        }
        if (dataContext.getResult() != null) {
            if (dataContext.getResult() instanceof Collection<?>) {
                dataContext.setResult(mapper.convertToDtos((List<E>) dataContext.getResult()));
            } else if (dataContext.getResult() instanceof EntityDocument) {
                dataContext.setResult(mapper.convertToDto((E) dataContext.getResult()));
            }
        }
        return dataContext;
    }

    public <E, D, M extends BaseMapStruct<D, E>> DataContext convertToEntity(final DataContext dataContext, final Class<M> mapperClass) {
        final M mapper = Mappers.getMapper(mapperClass);
        if (dataContext.getObject() != null) {
            if (dataContext.getObject() instanceof Collection<?>) {
                dataContext.setObject(mapper.convertToEntities((List<D>) dataContext.getObject()));
            } else {
                dataContext.setObject(mapper.convertToEntity((D) dataContext.getObject()));
            }
        }

        return dataContext;
    }

    public <E, D, M extends BaseMapStruct<D, E>> Collection<D> convertToDtos(final Collection<E> entityCollection, final Class<M> mapperClass) {
        return Mappers.getMapper(mapperClass).convertToDtos(entityCollection);
    }

    public <E, D, M extends BaseMapStruct<D, E>> Collection<E> convertToEntities(final Collection<D> dtoCollection, final Class<M> mapperClass) {
        return Mappers.getMapper(mapperClass).convertToEntities(dtoCollection);
    }

}

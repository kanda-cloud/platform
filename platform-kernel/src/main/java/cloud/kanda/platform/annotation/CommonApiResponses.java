package cloud.kanda.platform.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.http.MediaType;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@ApiResponses(value = {
		@ApiResponse(responseCode = "200", 
					 description = "Request completed succesfully", 
					 content = { 
							 @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
							 }),
        @ApiResponse(responseCode = "400", 
        			 description = "Bad Request",
					 content = { 
							 @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
//									  schema = @Schema(implementation = ErrorCode.class))
							 }),
        @ApiResponse(responseCode = "401",
        			 description = "Unauthorized: the request requires user authentication",
        			 content = { 
        					 @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
//        							  schema = @Schema(implementation = ExternalErrorCode.class))
        					 }),
        @ApiResponse(responseCode = "403",
        			 description = "Forbidden",
        			 content = { 
        					 @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
//        							  schema = @Schema(implementation = ExternalErrorCode.class))
        					 })
})
public @interface CommonApiResponses {
}

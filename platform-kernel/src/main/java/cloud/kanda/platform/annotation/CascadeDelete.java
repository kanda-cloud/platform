package cloud.kanda.platform.annotation;

import cloud.kanda.platform.entity.EntityDocument;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Interface CascadeDelete.
 * Include referenced classes to delete entities too when the entity annotated is deleted
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface CascadeDelete {

    Class<? extends EntityDocument<String>>[] classes();

}

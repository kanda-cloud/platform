package cloud.kanda.platform.service.imp;

import cloud.kanda.platform.entity.BaseEntity;
import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.repository.RepositoryCustom;
import cloud.kanda.platform.service.AbstractService;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import cloud.kanda.platform.utils.reflex.UtilsReflex;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The class AbstractServiceImp.
 *
 * @param <R> the type repository
 * @param <E> the type entity
 * @param <I> the type identifier
 */
@RequiredArgsConstructor
@Slf4j
public abstract class AbstractServiceImp<R extends RepositoryCustom & MongoRepository<E, I>,
        E extends EntityDocument<I>,
        I extends Serializable>
        implements AbstractService<E, I> {

    protected final R repository;

    @Override
    public long count() {
        return this.repository.count();
    }

    @Transactional
    @Override
    public void delete(final E entity) {
        this.repository.delete(entity);
    }

    @Transactional
    @Override
    public void delete(final I id) {
        this.repository.deleteById(id);
    }

    @Transactional
    @Override
    public void delete(final Collection<I> ids) {
        this.repository.deleteAllById(ids);
    }

    @Override
    public boolean exists(final I id) {
        return this.repository.existsById(id);
    }

    @Override
    public Collection<E> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Collection<E> findAll(final Collection<I> ids) {
        return (Set<E>) this.repository.findAllById(ids);
    }

    @Override
    public E findOne(final I id) {
        return this.findOneOptional(id).orElse(null);
    }

    private Optional<E> findOneOptional(final I id) {
        return this.repository.findById(id);
    }

    @Transactional
    @Override
    public E insert(final E entity) {
        return this.repository.insert(entity);
    }

    @Transactional
    @Override
    public Collection<E> insert(final Collection<E> entities) {
        return this.repository.insert(entities);
    }

    @Transactional
    @Override
    public E partialUpdate(final Map<String, Object> parameters, final I id) {
        return this.findOneOptional(id).map(entityFound -> {
            E entity = null;
            try {
                final E entityTemp = this.performPartialUpdate(parameters, entityFound, Boolean.FALSE);
                entity = this.save(entityTemp);
            } catch (final Exception e) {
                log.debug(e.getMessage());
            }

            return entity;

        }).orElse(null);
    }

    private E performPartialUpdate(final Map<String, Object> parameters, final E entity, final boolean saveNulls) throws Exception {
        final Map<String, Field> mapFields = UtilsReflex.parseListFieldsToMap(UtilsReflex.getFieldsClass(entity.getClass(), Boolean.TRUE));
        boolean hasChanged = Boolean.FALSE;
        for (final Entry<String, Object> entry : parameters.entrySet()) {
            if ((saveNulls && entry.getValue() == null) || entry.getValue() != null) {
                final Field field;
                try {
                    field = mapFields.get(entry.getKey());
                    if (field.get(entity) != entry.getValue()) {
                        hasChanged = Boolean.TRUE;
                        field.set(entity, entry.getValue());
                    }
                } catch (final SecurityException | IllegalArgumentException | IllegalAccessException e) {
                    log.error(e.getMessage());
                }
            }
        }

        if (!hasChanged) {
            throw new Exception("La entidad no tiene cambios para actualizar");
        }

        return entity;
    }

    @Transactional
    @Override
    public Collection<E> partialUpdate(final Map<I, Map<String, Object>> parameters) {
        return parameters.entrySet().stream().map(entry -> this.partialUpdate(entry.getValue(), entry.getKey())).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public E save(final E entity) {
        return this.save(entity, entity.getId());
    }

    private E save(final E entity, final I id) {
        return this.findOneOptional(id).map(entityFound -> {
            try {
                return this.performSave(entity, entityFound);
            } catch (final Exception e) {
                log.debug(e.getMessage());
            }
            return null;

        }).orElse(null);
    }

    private E performSave(final E entityToSave, final E entityOriginal) throws Exception {
        final Collection<Field> mapFields = UtilsReflex.getFieldsClass(entityToSave.getClass(), Boolean.TRUE);
        boolean hasChanged = Boolean.FALSE;
        for (final Field field : mapFields) {
            ReflectionUtils.makeAccessible(field);
            if (field.getName().equals("serialVersionUID") || field.getName().equals(BaseEntity.Fields.version)) {
                continue;
            }
            if ((field.get(entityOriginal) != null && !field.get(entityOriginal).equals(field.get(entityToSave)))
                    || (field.get(entityOriginal) == null && field.get(entityToSave) != null)) {
                hasChanged = Boolean.TRUE;
                field.set(entityOriginal, field.get(entityToSave));
            }
        }

        if (!hasChanged) {
            throw new Exception("La entidad no tiene cambios para actualizar");
        }

        return this.repository.save(entityOriginal);
    }

    @Transactional
    @Override
    public Collection<E> save(final Collection<E> entities) {
        return entities.stream().map(this::save).collect(Collectors.toList());
    }

    @Override
    public DataContext count(final DataContext dataContext) {
        final DataContext dataContextResult = this.repository.count(dataContext);
        dataContextResult.cleanCriteriaData();

        return dataContextResult;
    }

    @Override
    public DataContext exists(final DataContext dataContext) {
        final DataContext dataContextResult = this.repository.exists(dataContext);
        dataContextResult.cleanCriteriaData();

        return dataContextResult;
    }

    @Override
    public DataContextPageable findAll(final DataContextPageable dataContext) {
        final DataContextPageable dataContextResult = this.repository.findAll(dataContext);
        dataContextResult.cleanCriteriaData();

        return dataContextResult;
    }

    @Override
    public DataContext findOne(final DataContext dataContext) {
        final DataContext dataContextResult = this.repository.findOne(dataContext);
        dataContextResult.cleanCriteriaData();

        return dataContextResult;
    }

}

package cloud.kanda.platform.service;

import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public interface AbstractService<E extends EntityDocument<I>,
        I extends Serializable> {

    /**
     * Count.
     *
     * @param dataContext the dataContext
     * @return the DataContext
     */
    DataContext count(@NotNull DataContext dataContext);

    /**
     * Exists.
     *
     * @param dataContext the dataContext
     * @return the DataContext
     */
    DataContext exists(@NotNull DataContext dataContext);

    /**
     * Find all.
     *
     * @param dataContext the dataContext
     * @return the DataContext
     */
    DataContextPageable findAll(@NotNull DataContextPageable dataContext);

    /**
     * Find one.
     *
     * @param dataContext the dataContext
     * @return the DataContext
     */
    DataContext findOne(@NotNull DataContext dataContext);

    /**
     * Count.
     *
     * @return the long
     */
    long count();

    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(@NotNull E entity);

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(@NotNull I id);

    /**
     * Delete.
     *
     * @param ids the entity ids to delete
     */
    void delete(@NotEmpty Collection<I> ids);

    /**
     * Exists.
     *
     * @param id the id
     * @return true, if exists
     */
    boolean exists(@NotNull I id);

    /**
     * Find all.
     *
     * @return the list
     */
    Collection<E> findAll();

    /**
     * Find all.
     *
     * @param ids the ids
     * @return the list
     */
    Collection<E> findAll(@NotEmpty Collection<I> ids);

    /**
     * Find one.
     *
     * @param id the id
     * @return the e
     */
    E findOne(@NotNull I id);

    /**
     * Insert.
     *
     * @param entity the entity
     * @return the e
     */
    E insert(@NotNull E entity);

    /**
     * Insert.
     *
     * @param entities the entities
     * @return the list
     */
    Collection<E> insert(@NotEmpty Collection<E> entities);

    /**
     * Save.
     *
     * @param entity the entity
     * @return the e
     */
    E save(@NotNull E entity);

    /**
     * Save.
     *
     * @param entities the entities
     * @return the list
     */
    Collection<E> save(@NotEmpty Collection<E> entities);

    /**
     * Partial update.
     *
     * @param parameters the fields/value of entity to update
     * @param id         the id entity to update
     * @return the e
     */
    E partialUpdate(@NotEmpty Map<String, Object> parameters, @NotNull I id);

    /**
     * Partial update.
     *
     * @param parameters map id entity to update and the fields/value of entity to update
     * @return the list
     */
    Collection<E> partialUpdate(@NotEmpty Map<I, Map<String, Object>> parameters);

}

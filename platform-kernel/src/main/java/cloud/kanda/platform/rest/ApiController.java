package cloud.kanda.platform.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cloud.kanda.platform.annotation.CommonApiResponses;
import cloud.kanda.platform.configurations.openapi.OpenApiConfig;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "API Info", description = "Information about API")
public class ApiController {
	
	@Value("${build.version}")
	private String BUILD_VERSION;
	
	private Boolean maintenance = Boolean.FALSE;
	
	private final SimpMessagingTemplate template;
	
	@GetMapping("/build-version")
	@ResponseStatus(code = HttpStatus.OK)
	@Operation(summary = "Get version API")
	@ApiResponse(responseCode = "200", description = "Number version API", 
		  	     content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, 
		  	   						  schema = @Schema(implementation = String.class))})
	public ResponseEntity<String> buildVersion() {
		return ResponseEntity.ok(this.BUILD_VERSION);
	}
	
	@GetMapping("/ping")
	@ResponseStatus(code = HttpStatus.OK)
	@Operation(summary = "Responds if API is available")
	@ApiResponses(value = { 
		@ApiResponse(responseCode = "200", description = "API available", 
				     content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, 
				   						  schema = @Schema(implementation = Boolean.class))}),
		@ApiResponse(responseCode = "404", description = "API not available", content = @Content)
	})
	public ResponseEntity<Boolean> ping() {
		return ResponseEntity.ok(Boolean.TRUE);
	}
	
	@GetMapping("/is-maintenance-mode")
	@ResponseStatus(code = HttpStatus.OK)
	@Operation(summary = "Responds if API is in maintenance mode")
	@ApiResponses(value = { 
		@ApiResponse(responseCode = "200", description = "Maintenance mode", 
				     content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, 
				   						  schema = @Schema(implementation = Boolean.class))})
	})
	public ResponseEntity<Boolean> isMaintenanceMode() {
		return ResponseEntity.ok(this.maintenance);
	}
	
	@PostMapping("/toggle-maintenance-mode")
	@ResponseStatus(code = HttpStatus.OK)
	@SecurityRequirement(name = OpenApiConfig.KEYCLOAK_OAUTH)
	@Operation(summary = "Toggle maintenance mode")
	@CommonApiResponses
	public ResponseEntity<Void> toggleMaintenanceMode() {
		this.maintenance = !this.maintenance;
		this.template.convertAndSend("/get-maintenance/", this.maintenance);
		
		return ResponseEntity.ok().build();
	}
	
}

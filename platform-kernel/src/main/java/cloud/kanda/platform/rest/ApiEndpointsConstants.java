package cloud.kanda.platform.rest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ApiEndpointsConstants {
    public final String COUNT_BY = "/count-by";
    public final String EXISTS_BY = "/exists-by";
    public final String FIND_ALL_BY = "/find-all-by";
    public final String FIND_ONE_BY = "/find-one-by";

    public final String SLASH = "/";
    public final String ALL = "/all";
    public final String BULK_SAVE = "/bulk-save";
    public final String BULK_DELETE = "/bulk-delete";
    public final String COUNT = "/count";
    public final String EXISTS_ID = "/exists-id";

}

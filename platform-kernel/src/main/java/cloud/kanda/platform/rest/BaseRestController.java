package cloud.kanda.platform.rest;

import cloud.kanda.platform.annotation.CommonApiResponses;
import cloud.kanda.platform.dto.BaseDto;
import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.mappers.BaseMapStruct;
import cloud.kanda.platform.service.AbstractService;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

@SuppressWarnings(value = {"unchecked"})
public abstract class BaseRestController<S extends AbstractService<E, I>,
        D extends BaseDto,
        E extends EntityDocument<I>,
        M extends BaseMapStruct<D, E>,
        I extends Serializable> {

    @Autowired
    protected S service;

    @Autowired
    protected M mapStruct;

    /* DataContext */

    @PostMapping(ApiEndpointsConstants.COUNT_BY)
    @Operation(description = "Count elements filtered by dataContext", summary = "Count elements", tags = {"DataContext"})
    @CommonApiResponses
    public ResponseEntity<DataContext> countBy(@Parameter(description = "DataContext with filter")
                                               @RequestBody final DataContext dataContext) {
        return ResponseEntity.ok(this.service.count(this.convertToEntity(dataContext)));
    }

    @PostMapping(ApiEndpointsConstants.EXISTS_BY)
    @Operation(description = "Exists elements filtered by dataContext", summary = "Exists elements", tags = {"DataContext"})
    @CommonApiResponses
    public ResponseEntity<DataContext> existsBy(@RequestBody final DataContext dataContext) {
        return ResponseEntity.ok(this.convertToDto(this.service.exists(this.convertToEntity(dataContext))));
    }

    @PostMapping(ApiEndpointsConstants.FIND_ALL_BY)
    @Operation(description = "Find all elements filtered by dataContext", summary = "Find elements", tags = {"DataContextPageable"})
    @CommonApiResponses
    public ResponseEntity<DataContextPageable> findAllBy(@RequestBody final DataContextPageable dataContext) {
        return ResponseEntity.ok((DataContextPageable) this.convertToDto(this.service.findAll((DataContextPageable) this.convertToEntity(dataContext))));
    }

    @PostMapping(ApiEndpointsConstants.FIND_ONE_BY)
    @Operation(description = "Find a element filtered by dataContext", summary = "Find element", tags = {"DataContext"})
    @CommonApiResponses
    public ResponseEntity<DataContext> findOneBy(@RequestBody final DataContext dataContext) {
        return ResponseEntity.ok(this.convertToDto(this.service.findOne(this.convertToEntity(dataContext))));
    }


    /* RESTFUL */

    @DeleteMapping("/{id}")
    @Operation(description = "Delete entity by identifier", summary = "Delete by id")
    @CommonApiResponses
    @ApiResponse(responseCode = "204", description = "No content to delete", content = @Content)
    public ResponseEntity<D> delete(@PathVariable final I id) {
        final D objectTemp = this.convertToDto(this.service.findOne(id));
        if (objectTemp == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        this.service.delete(id);

        return ResponseEntity.ok(objectTemp);
    }

    @PutMapping(ApiEndpointsConstants.BULK_DELETE)
    @Operation(description = "Remove entities", summary = "Massive delete")
    @CommonApiResponses
    public ResponseEntity<Collection<D>> bulkDelete(@RequestBody final Collection<I> ids) {
        this.service.delete(ids);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    @Operation(description = "Find entity by identifier", summary = "Find by id")
    @CommonApiResponses
    public ResponseEntity<D> find(@PathVariable final I id) {
        return ResponseEntity.ok(this.convertToDto(this.service.findOne(id)));
    }

    @GetMapping
    @Operation(description = "Find all entities", summary = "Find all")
    @CommonApiResponses
    public ResponseEntity<Collection<D>> find() {
        return ResponseEntity.ok(this.convertToDto(this.service.findAll()));
    }

    @PostMapping
    @Operation(description = "Create new entity", summary = "Create entity")
    @CommonApiResponses
    @ApiResponse(responseCode = "201", description = "Entity created",
            content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE)})
    public ResponseEntity<D> insert(@NotNull @RequestBody final D object) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.convertToDto(this.service.insert(this.convertToEntity(object))));
    }

    @PutMapping
    @Operation(description = "Update entity by identifier", summary = "Update by id")
    @CommonApiResponses
    @ApiResponse(responseCode = "204", description = "No content to update", content = @Content)
    public ResponseEntity<D> save(@Valid @RequestBody final D object) {
        final D objectTemp = this.convertToDto(this.service.save(this.convertToEntity(object)));
        if (objectTemp == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok(objectTemp);
    }

    @PutMapping(ApiEndpointsConstants.BULK_SAVE)
    @Operation(description = "Update entities", summary = "Bulk update")
    @CommonApiResponses
    @ApiResponse(responseCode = "204", description = "No content to update", content = @Content)
    public ResponseEntity<Collection<D>> bulkSave(@RequestBody final Collection<D> objects) {
        final Collection<D> objectsTemp = this.convertToDto(this.service.save(this.convertToEntity(objects)));
        if (objectsTemp.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok(objectsTemp);
    }

    @PatchMapping("/{id}")
    @Operation(description = "Update attributes entity by identifier", summary = "Partial update")
    @CommonApiResponses
    @ApiResponse(responseCode = "204", description = "No content to update", content = @Content)
    public ResponseEntity<D> partialUpdate(@RequestBody final Map<String, Object> parameters, @PathVariable final I id) {
        final D objectTemp = this.convertToDto(this.service.partialUpdate(parameters, id));
        if (objectTemp == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok(objectTemp);
    }


    /* OTHERS */

    @GetMapping(ApiEndpointsConstants.COUNT)
    @Operation(description = "Count all entities", summary = "Count all")
    @CommonApiResponses
    public ResponseEntity<Long> count() {
        return ResponseEntity.ok(this.service.count());
    }

    @GetMapping(ApiEndpointsConstants.EXISTS_ID + "/{id}")
    @Operation(description = "Exists entity by identifier", summary = "Exists by id")
    @CommonApiResponses
    public ResponseEntity<Boolean> existsId(@PathVariable final I id) {
        return ResponseEntity.ok(this.service.exists(id));
    }

    @PatchMapping(ApiEndpointsConstants.ALL)
    @Operation(description = "Update attributes entities", summary = "Massive partial update")
    @CommonApiResponses
    @ApiResponse(responseCode = "204", description = "No content to update", content = @Content)
    public ResponseEntity<Collection<D>> partialUpdate(@RequestBody final Map<I, Map<String, Object>> parameters) {
        final Collection<D> objectTemp = this.convertToDto(this.service.partialUpdate(parameters));
        if (objectTemp == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok(objectTemp);
    }


    /* MapStruct */

    protected DataContext convertToDto(final DataContext dataContext) {
        if (dataContext.getObject() != null) {
            if (dataContext.getObject() instanceof Collection<?>) {
                dataContext.setObject(this.convertToDto((Collection<E>) dataContext.getObject()));
            } else {
                dataContext.setObject(this.convertToDto((E) dataContext.getObject()));
            }
        }
        if (dataContext.getResult() != null) {
            if (dataContext.getResult() instanceof Collection<?>) {
                dataContext.setResult(this.convertToDto((Collection<E>) dataContext.getResult()));
            } else if (dataContext.getResult() instanceof EntityDocument) {
                dataContext.setResult(this.convertToDto((E) dataContext.getResult()));
            }
        }
        dataContext.setClazzObject(dataContext.getObject().getClass().getName());

        return dataContext;
    }

    protected DataContext convertToEntity(final DataContext dataContext) {
        if (dataContext.getObject() != null) {
            if (dataContext.getObject() instanceof Collection<?>) {
                dataContext.setObject(this.convertToEntity((Collection<D>) dataContext.getObject()));
            } else {
                dataContext.setObject(this.convertToEntity((D) dataContext.getObject()));
            }
        }

        return dataContext;
    }

    protected D convertToDto(final E entity) {
        return this.mapStruct.convertToDto(entity);
    }

    protected E convertToEntity(final D dto) {
        return this.mapStruct.convertToEntity(dto);
    }

    protected Collection<D> convertToDto(final Collection<E> entityCollection) {
        return this.mapStruct.convertToDtos(entityCollection);
    }

    protected Collection<E> convertToEntity(final Collection<D> dtoCollection) {
        return this.mapStruct.convertToEntities(dtoCollection);
    }

}

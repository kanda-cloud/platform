package cloud.kanda.platform.utils.data.deserialize;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import cloud.kanda.platform.repository.imp.CriteriaOperatorEnum;
import cloud.kanda.platform.utils.data.CriteriaCombination;

public class CriteriaCombinationGsonDeserialize implements JsonDeserializer<CriteriaCombination> {
	
	@Override
	public CriteriaCombination deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		CriteriaCombination criteriaCombination = new CriteriaCombination();
		Gson gson = new Gson();

		JsonObject criteriaCombinationJson = json.getAsJsonObject();
		
		JsonElement first = criteriaCombinationJson.get(CriteriaCombination.Fields.first);
		if (first != null) {
			if (first.isJsonObject()) {
				criteriaCombination.setFirstCombination(gson.fromJson(first, CriteriaCombination.class));
			} else {
				criteriaCombination.setFirst(gson.fromJson(first, Long.class));
			}
		}
		
		JsonElement second = criteriaCombinationJson.get(CriteriaCombination.Fields.second);
		if (second != null) {
			if (second.isJsonObject()) {
				criteriaCombination.setSecondCombination(gson.fromJson(second, CriteriaCombination.class));
			} else {
				criteriaCombination.setSecond(gson.fromJson(second, Long.class));
			}
		}
		
		criteriaCombination.setOperator(gson.fromJson(criteriaCombinationJson.get(CriteriaCombination.Fields.operator), CriteriaOperatorEnum.class));

		return criteriaCombination;
	}

}

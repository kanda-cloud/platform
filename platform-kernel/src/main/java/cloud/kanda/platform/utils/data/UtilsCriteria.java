package cloud.kanda.platform.utils.data;

import cloud.kanda.platform.entity.BaseEntity;
import cloud.kanda.platform.repository.imp.CriteriaOperatorEnum;
import cloud.kanda.platform.utils.reflex.UtilsReflex;
import lombok.experimental.UtilityClass;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@UtilityClass
@SuppressWarnings("unchecked")
public class UtilsCriteria {

    public final String SERIAL_VERSION_UID = "serialVersionUID";
    public final Long LEVEL_IMPLICIT = -1L;
    public final Long ORDER_AUDIT = -1L;

    public Map<String, CriteriaOperatorEnum> getSentencesExcludeAudit() {
        final Map<String, CriteriaOperatorEnum> sentences = new HashMap<>();
        sentences.put(BaseEntity.Fields.createdBy, CriteriaOperatorEnum.EXCLUDE);
        sentences.put(BaseEntity.Fields.lastModifiedBy, CriteriaOperatorEnum.EXCLUDE);
        sentences.put(BaseEntity.Fields.createdDate, CriteriaOperatorEnum.EXCLUDE);
        sentences.put(BaseEntity.Fields.lastModifiedDate, CriteriaOperatorEnum.EXCLUDE);
        sentences.put(BaseEntity.Fields.version, CriteriaOperatorEnum.EXCLUDE);

        return sentences;
    }

    public void addCriteriaDataLevelImplicit(final Map<Long, List<CriteriaData>> levelCriteriaData, final CriteriaData criteriaData) {
        if (levelCriteriaData.containsKey(LEVEL_IMPLICIT)) {
            levelCriteriaData.get(LEVEL_IMPLICIT).add(criteriaData);
        } else {
            final List<CriteriaData> listCriteriaData = new ArrayList<>();
            listCriteriaData.add(criteriaData);
            levelCriteriaData.put(LEVEL_IMPLICIT, listCriteriaData);
        }
    }

    public CriteriaData generateCriteriaData(final Long order, final CriteriaOperatorEnum operator, final Map<String, CriteriaOperatorEnum> sentences) {
        return CriteriaData.builder()
                .order(order)
                .operator(operator)
                .sentences(sentences)
                .build();
    }

    public Map<String, CriteriaOperatorEnum> getSentencesDefault(final Object object) {
        final Map<String, CriteriaOperatorEnum> sentences = new HashMap<>();
        final Class<?> _class = object.getClass();
        final Collection<Field> fields = UtilsReflex.getFieldsClass(_class, Boolean.TRUE);

        fields.forEach(field -> {
            sentences.put(field.getName(), CriteriaOperatorEnum.EQUALS);
        });

        return sentences;
    }

    public Object getValueCriteriaMutation(final Object object, final Field field) throws IllegalArgumentException, IllegalAccessException {

        // Si hay valor en el campo original tiene preferencia sobre el mutation
        if (field.get(object) != null) {
            return getValueCriteriaField(object, field);
        }

        final Class<?> _class = object.getClass();
        final Collection<Field> fields = UtilsReflex.getFieldsClass(_class, Boolean.TRUE);

        // Obtener el valor del campo
        Object value = null;

        try {

            // Recorrer los campos del objeto para aplicar los criterios
            for (final Field fieldTemp : fields) {
                ReflectionUtils.makeAccessible(fieldTemp);

                // Si es el serial version se omite
                if (fieldTemp.getName().equals(SERIAL_VERSION_UID)) {
                    continue;
                }

                // Si el campo indicado en la anotación CriteriaMutation se obtiene el valor
                if (fieldTemp.getName().equals(field.getAnnotation(CriteriaMutation.class).fieldValue())) {
                    value = fieldTemp.get(object);
                    break;
                }

            }

        } catch (final IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return value;

    }

    public boolean canDoCriteria(final Entry<String, CriteriaOperatorEnum> entrySentence, final Object value) {
        // Si el campo tiene valor
        if (value != null && !value.equals("") && !entrySentence.getValue().equals(CriteriaOperatorEnum.EXCLUDE)
                && (!(value instanceof Collection)
                || (value instanceof Collection
                && (!((Collection<?>) value).isEmpty() || entrySentence.getValue().equals(CriteriaOperatorEnum.EMPTY))))) {
            return Boolean.TRUE;
        } // Si el campo no tiene valor, pero el criterio es nulo o vacío
        else if (entrySentence.getValue().equals(CriteriaOperatorEnum.NULLABLE)
                || entrySentence.getValue().equals(CriteriaOperatorEnum.EMPTY)) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;

    }

    public Map<String, Object> getFieldsValues(final Object object, final Collection<Field> fields) {
        final Map<String, Object> fieldValues = new HashMap<>();
        // Recorrer los campos del objeto para aplicar los criterios
        for (final Field field : fields) {

            ReflectionUtils.makeAccessible(field);

            // Si es el serial version o es un campo con la anotación CriteriaExclusion, se omite
            if (field.getName().equals(SERIAL_VERSION_UID) || field.isAnnotationPresent(CriteriaExclusion.class)) {
                continue;
            }

            // Obtener el valor del campo
            Object value = null;
            try {
                // Si incluye la anotación CriteriaMutation, el valor del campo no es el del campo
                // sino el del campo indicado en la anotación como fieldValue
                if (field.isAnnotationPresent(CriteriaMutation.class)) {
                    value = getValueCriteriaMutation(object, field);
                } else if (field.get(object) != null) {
                    value = getValueCriteriaField(object, field);
                }

            } catch (final IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            } finally {
                fieldValues.put(field.getName(), value);
            }

        }

        return fieldValues;
    }

    private Object getValueCriteriaField(final Object object, final Field field)
            throws IllegalAccessException {
        // Obtener valor si es un enum
        if (field.getType().isEnum()) {
            return field.get(object).toString();
        } // Obtener el valor de un campo
        if (!field.get(object).getClass().equals(HashSet.class)) {
            return field.get(object);
        }
        if (field.get(object).getClass().equals(HashSet.class)) {
            return new ArrayList<>((HashSet<Object>) field.get(object));
        }

        return null;
    }

    public Query getQuery(final DataContext dataContext) {
        final Query query = new Query();

        // FIXME: comentado debería llegar siempre iniciado
//		if (dataContext.getCriteriaContent() == null) {
//			dataContext.setCriteriaContent(new CriteriaContent());
//		}
        if ((dataContext.getLevelCriteriaData().isEmpty())
                || (dataContext.getCriteriaContent().getLevelCriteriaData().size() == 1 && dataContext.getLevelCriteriaData().containsKey(LEVEL_IMPLICIT))) {
            dataContext.getCriteriaContent().generateDefaultCriteriaObject(dataContext.getObject());
        }

        final Criteria criteria = criteria(dataContext);

        // Si hay filtros que aplicar se añaden
        if (criteria != null) {
            query.addCriteria(criteria);
        }

        applyPagination(dataContext, query);

        return query;
    }

    private void applyPagination(final DataContext dataContext, final Query query) {
        // Si existe paginación se aplica
        if (dataContext instanceof final DataContextPageable dataContextPageable) {

            if (dataContextPageable.getPageable() != null) {

                query.with(dataContextPageable.getPageable());

                // Si existe orden de campos se aplica
                if (dataContextPageable.getPageable().getSort().isSorted()) {
                    query.with(dataContextPageable.getPageable().getSort());
                }
            }
        }
    }

    private Criteria criteria(final DataContext dataContext) {
        // Ordenar niveles de los criterios
        dataContext.getLevelCriteriaData()
                .entrySet()
                .stream()
                .sorted(Map.Entry.<Long, List<CriteriaData>>comparingByKey());

        // Añadir criteria del nivel implicito
        addCriteriaLevelImplicit(dataContext);

        // Calcular criterios de nivel-orden-criteria
        final Map<Long, Map<Long, Criteria>> levelOrderCriteria = getCriteriaLevelsOrdersSentences(dataContext);

        // Calcular operadores entre ordenes de niveles
        final Map<Long, Criteria> levelCriteria = getCriteriaLevelsOrders(dataContext.getLevelCriteriaOrderCombination(), levelOrderCriteria);

        // Calcular operadores entre niveles
        return getCriteriaLevels(dataContext.getLevelCriteriaCombination(), levelCriteria);
    }

    private void addCriteriaLevelImplicit(final DataContext dataContext) {
        // Si viene el nivel -1, quiere decir que incluye el nivel implicito y las condiciones recibidas son aplicadas obligatoriamente
        if (dataContext.getLevelCriteriaData().containsKey(LEVEL_IMPLICIT)) {

            final List<CriteriaData> criteriasDataImplicit = dataContext.getLevelCriteriaData().get(LEVEL_IMPLICIT);
            final List<CriteriaCombination> criteriasCombination = new ArrayList<>();
            boolean isFirst = Boolean.TRUE;
            CriteriaCombination lastCombination = null;
            CriteriaData firstCriteriaData = null;

            // Combinar todos los ordenes del nivel -1 con operador AND y combinar todas las combinaciones
            // Ordenes -1 AND 0, $-10 AND 2...
            for (final CriteriaData criteriaData : criteriasDataImplicit) {
                if (isFirst) {
                    if (criteriasDataImplicit.size() == 1) {
                        criteriasCombination.add(CriteriaCombination.builder().first(criteriaData.getOrder()).operator(CriteriaOperatorEnum.AND).build());
                        continue;
                    } else {
                        isFirst = Boolean.FALSE;
                        firstCriteriaData = criteriaData;
                    }
                }
                if (lastCombination == null) {
                    lastCombination = CriteriaCombination.builder().first(firstCriteriaData.getOrder()).second(criteriaData.getOrder()).operator(CriteriaOperatorEnum.AND).build();
                    criteriasCombination.add(lastCombination);
                } else {
                    lastCombination = CriteriaCombination.builder().firstCombination(lastCombination).second(criteriaData.getOrder()).operator(CriteriaOperatorEnum.AND).build();
                    criteriasCombination.add(lastCombination);
                }
            }

            dataContext.getLevelCriteriaOrderCombination().put(LEVEL_IMPLICIT, criteriasCombination);
            // FIXME - puede que el nivel 0 ya este combinado con otro nivel, fallaría la combinacion de -1 y 0?
            // Combinar el nivel implicito -1 al nivel 0
            dataContext.getLevelCriteriaCombination().add(CriteriaCombination.builder().first(LEVEL_IMPLICIT).second(0L).operator(CriteriaOperatorEnum.AND).build());
        }
    }

    private Map<Long, Map<Long, Criteria>> getCriteriaLevelsOrdersSentences(final DataContext dataContext) {
        final Map<Long, Map<Long, Criteria>> levelCriteriaPreBuilded = dataContext.getLevelCriteriaPreBuilded();
        final Map<Long, List<CriteriaData>> levelCriteriaData = dataContext.getLevelCriteriaData();
        final Map<Long, Map<Long, Criteria>> levelOrderCriteria = new HashMap<>();

        // Recorrer los niveles
        for (final Entry<Long, List<CriteriaData>> entryLevels : levelCriteriaData.entrySet()) {
            final Long level = entryLevels.getKey();
            final Map<Long, Criteria> orderCriteria;

            // TODO: ahora mismo se aplica el nivel preconstruido, incluir funcionalidad para combinar el preconstruido más el que se calcule
            // Si el nivel ya está preconstruido, se aplica
            if (levelCriteriaPreBuilded.containsKey(level)) {
                orderCriteria = levelCriteriaPreBuilded.get(level);
            } else {
                orderCriteria = buildCriteriaLevel(dataContext, entryLevels);
            }
            levelOrderCriteria.put(level, orderCriteria);
        }

        return levelOrderCriteria;
    }

    public Map<Long, Criteria> buildCriteriaLevel(final DataContext dataContext,
                                                  final Entry<Long, List<CriteriaData>> entryLevels) {
        final Map<Long, Criteria> orderCriteria = new HashMap<>();
        final Collection<Field> fields = UtilsReflex.getFieldsClass(dataContext.getObjectClass(), Boolean.TRUE);
        final Map<String, Object> fieldsValues = getFieldsValues(dataContext.getObject(), fields);

        // Ordenar los criterios por el campo order
        Collections.sort(entryLevels.getValue());

        // Recorrer los criterios del nivel
        for (final CriteriaData criteriaData : entryLevels.getValue()) {
            final Collection<Criteria> criterias = new ArrayList<>();
            // Recorrer las sentencias
            for (final Entry<String, CriteriaOperatorEnum> entrySentence : criteriaData.getSentences().entrySet()) {
                // Comprobar si el Field es apto para criteria
                if (UtilsCriteria.canDoCriteria(entrySentence, fieldsValues.get(entrySentence.getKey()))) {
                    // Obtener criteria
                    final Criteria criteriaFieldTemp = criteria(entrySentence, fieldsValues.get(entrySentence.getKey()));
                    criterias.add(criteriaFieldTemp);
                }
            }
            orderCriteria.put(criteriaData.getOrder(), applyOperatorCriteriasIfNeeded(criteriaData, criterias));
        }

        return orderCriteria;
    }

    public Criteria applyOperatorCriteriasIfNeeded(final CriteriaData criteriaData,
                                                   final Collection<Criteria> criterias) {
        Criteria criteriaField = null;

        // Si hay varios criterios, se aplica el operador
        if (criterias.size() > 1) {
            if (criteriaData.getOperator() != null) {
                criteriaField = applyOperatorCriterias(criteriaData.getOperator(), Arrays.copyOf(criterias.toArray(), criterias.toArray().length, Criteria[].class));
            }
        } else if (!criterias.isEmpty()) {
            criteriaField = new ArrayList<>(criterias).get(0);
        }

        return criteriaField;
    }

    private Map<Long, Criteria> getCriteriaLevelsOrders(final Map<Long, List<CriteriaCombination>> levelsCriteriaOrdersCombination,
                                                        final Map<Long, Map<Long, Criteria>> levelsOrdersCriteria) {
        final Map<Long, Criteria> levelCriteria = new HashMap<>();

        // Si llegan criterios sin combinación de ordenes, se incluye por defecto nivel 0 con primer orden 0 y operador AND
        if (levelsCriteriaOrdersCombination.isEmpty() && !levelsOrdersCriteria.isEmpty()) {
            levelsCriteriaOrdersCombination.put(0L, Arrays.asList(CriteriaCombination.builder().first(0L).operator(CriteriaOperatorEnum.AND).build()));
        }

        // Recorrer las combinaciones de ordenes para los niveles
        for (final Entry<Long, List<CriteriaCombination>> levelOrdersCombination : levelsCriteriaOrdersCombination.entrySet()) {
            // Obtener unicamente los ordenes del nivel a combinar
            final Map<Long, Criteria> ordersCriteria = levelsOrdersCriteria.get(levelOrdersCombination.getKey());
            // Identificar la combinación absoluta
            final CriteriaCombination fullCriteriaCombination = getFullCriteriaCombination(levelOrdersCombination.getValue());
            // Combinar los criterios con los ordenes configurados para el nivel actual y guardar la combinacion por nivel
            levelCriteria.put(levelOrdersCombination.getKey(), getCriteriaCombinated(fullCriteriaCombination, ordersCriteria));
        }

        // Añadir las sentencias de los ordenes sin combinación
        for (final Entry<Long, Map<Long, Criteria>> levelOrderCriteria : levelsOrdersCriteria.entrySet()) {
            if (levelOrderCriteria.getValue().size() == 1) {
                levelCriteria.put(levelOrderCriteria.getKey(), levelOrderCriteria.getValue().get(0L));
            }
        }

        return levelCriteria;
    }

    private Criteria getCriteriaLevels(final List<CriteriaCombination> levelsCombination,
                                       final Map<Long, Criteria> levelCriteria) {
        // Si llegan criterios sin combinacion de niveles, se incluye por defecto nivel 0 y operador AND
        if (levelsCombination.isEmpty() && !levelCriteria.isEmpty()) {
            levelsCombination.add(CriteriaCombination.builder().first(0L).operator(CriteriaOperatorEnum.AND).build());
        }

        // Identificar la combinación absoluta
        final CriteriaCombination fullCriteriaCombination = getFullCriteriaCombination(levelsCombination);

        // Combinar los criterios con los niveles configurados
        return getCriteriaCombinated(fullCriteriaCombination, levelCriteria);
    }

    private CriteriaCombination getFullCriteriaCombination(final List<CriteriaCombination> criteriasCombination) {
        CriteriaCombination fullCriteriaCombination = null;
        final List<CriteriaCombination> criteriasCombinationClean = new ArrayList<>(criteriasCombination);

        // Recorrer todas las combinaciones
        for (final CriteriaCombination criteriaCombination : criteriasCombination) {
            boolean containsAllValues = Boolean.FALSE;
            // Obtener todos los valores utilizados recursivamente
            final List<Long> valuesUsed = getAllValuesUsed(criteriaCombination);
            // Eliminar la combinacion actual
            criteriasCombinationClean.remove(criteriaCombination);

            // Recorrer las combinaciones descartando las que se han evaluado
            for (final CriteriaCombination criteriaCombinationTemp : criteriasCombinationClean) {
                // Obtener todos los valores utilizados recursivamente
                final List<Long> valuesUsedTemp = getAllValuesUsed(criteriaCombinationTemp);
                // Si contiene todos los valores, quiere decir que alguna de las combinaciones restantes es superior a la actual
                if (valuesUsed.containsAll(valuesUsedTemp)) {
                    containsAllValues = true;
                    break;
                }
            }

            // Si no se han encontrado todas las combinaciones se guarda como posible combinacion absoluta
            if (!containsAllValues) {
                fullCriteriaCombination = criteriaCombination;
            }
        }

        return fullCriteriaCombination;
    }

    private List<Long> getAllValuesUsed(final CriteriaCombination criteriaCombinationTemp) {
        final List<Long> valuesUsed = new ArrayList<>();
        // Si es el valor es simple se añade, si es una combinación se llama recursivamente hasta obtener todos los valores posibles
        if (criteriaCombinationTemp.getFirst() != null) {
            valuesUsed.add(criteriaCombinationTemp.getFirst());
        } else if (criteriaCombinationTemp.getFirstCombination() != null) {
            valuesUsed.addAll(getAllValuesUsed(criteriaCombinationTemp.getFirstCombination()));
        }
        if (criteriaCombinationTemp.getSecond() != null) {
            valuesUsed.add(criteriaCombinationTemp.getSecond());
        } else if (criteriaCombinationTemp.getSecondCombination() != null) {
            valuesUsed.addAll(getAllValuesUsed(criteriaCombinationTemp.getSecondCombination()));
        }
        return valuesUsed;
    }

    private Criteria getCriteriaCombinated(final CriteriaCombination criteriaCombination, final Map<Long, Criteria> criterias) {
        Criteria criteriaCombinated = null;
        Criteria firstCriteria = null, secondCriteria = null;

        if (criteriaCombination != null) {
            // Si el valor es simple se añade el criterio, si es una combinación se llama recursivamente hasta obtener todos los criterios combinados
            if (criteriaCombination.getFirst() != null) {
                firstCriteria = criterias.get(criteriaCombination.getFirst());
            } else if (criteriaCombination.getFirstCombination() != null) {
                firstCriteria = getCriteriaCombinated(criteriaCombination.getFirstCombination(), criterias);
            }
            if (criteriaCombination.getSecond() != null) {
                secondCriteria = criterias.get(criteriaCombination.getSecond());
            } else if (criteriaCombination.getSecondCombination() != null) {
                secondCriteria = getCriteriaCombinated(criteriaCombination.getSecondCombination(), criterias);
            }

            // Si hay combinacion se unifican los criterios con el operador, si solo existe el primer o segundo criterio, se utiliza
            if (firstCriteria != null && secondCriteria != null) {
                criteriaCombinated = applyOperatorCriterias(criteriaCombination.getOperator(), new Criteria[]{firstCriteria, secondCriteria});
            } else if (firstCriteria != null) {
                criteriaCombinated = firstCriteria;
            } else if (secondCriteria != null) {
                criteriaCombinated = secondCriteria;
            }
        }

        return criteriaCombinated;
    }

    private Criteria applyOperatorCriterias(final CriteriaOperatorEnum criteriaOperator, final Criteria[] criterias) {
        final Criteria criteria = new Criteria();

        // Obtener el operador para unificar los criterios
        if (criteriaOperator == CriteriaOperatorEnum.OR) {
            return criteria.orOperator(criterias);
        } else {
            return criteria.andOperator(criterias);
        }
    }

    private Criteria criteria(final Entry<String, CriteriaOperatorEnum> entrySentence, final Object value) {
        Criteria criteria = Criteria.where(entrySentence.getKey());
        // Obtener los criterios de búsqueda
        switch (entrySentence.getValue()) {
            case EXCLUDE -> {
            }
            case EQUALS -> criteria = criteria.is(value);
            case NOT_EQUALS -> criteria = criteria.ne(value);
            case LIKE -> criteria = criteria.regex(value.toString());
            case START_WITH -> criteria = criteria.regex("^" + value);
            case END_WITH -> criteria = criteria.regex(value + "$");
            case LESS_THAN -> criteria = criteria.lt(value);
            case GREATHER_THAN -> criteria = criteria.gt(value);
            case LESS_EQUALS_THAN -> criteria = criteria.lte(value);
            case GREATHER_EQUALS_THAN -> criteria = criteria.gte(value);
            case COMBON_AND -> criteria = criteria.all(prepareValueList(value));
            case COMBO_OR -> criteria = criteria.in(prepareValueList(value));
            case NULLABLE -> criteria = criteria.exists(Boolean.FALSE);
            case EMPTY -> criteria = criteria.is(Strings.EMPTY);
            default -> criteria = criteria.is(value);
        }

        return criteria;
    }

    private Collection<Object> prepareValueList(final Object value) {
        Collection<Object> valueList = new ArrayList<>();

        if (!(value instanceof List)) {
            valueList.add(value);
        } else {
            valueList = (Collection<Object>) value;
        }

        return valueList;
    }

}

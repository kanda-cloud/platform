package cloud.kanda.platform.utils.data.deserialize;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public final class DateGsonDeserializer implements JsonDeserializer<Date>, JsonSerializer<Date> {
	
	static final DateTimeFormatter DATE_TIME_FORMATTER = ISODateTimeFormat.dateTime()
			.withZone(DateTimeZone.UTC);

	@Override
	public Date deserialize(final JsonElement json, final Type type, final JsonDeserializationContext jdc)
			throws JsonParseException {
		if (json.isJsonObject() && json.getAsJsonObject().get("$date") != null) {
			return new Date(json.getAsJsonObject().get("$date").getAsLong());
		} else if (json.isJsonPrimitive()) {
			if (NumberUtils.isCreatable(json.getAsString())) {
				return new Date(json.getAsLong());
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy, hh:mm:ss a");
				try {
					return new Date(simpleDateFormat.parse(json.getAsString()).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public JsonElement serialize(Date date, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(date.getTime());
	}
	
}
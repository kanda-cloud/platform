package cloud.kanda.platform.utils.data;

import java.util.Map;

import cloud.kanda.platform.repository.imp.CriteriaOperatorEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Schema(name = "CriteriaData", description = "Information to generate a partial filter with criteria")
@Getter
@Setter
@Builder
public class CriteriaData implements Comparable<CriteriaData> {
	
	@Schema(name = "sentences", description = "Operators to apply to attributes")
	private Map<String, CriteriaOperatorEnum> sentences;
	
	@Schema(name = "operator", description = "Operator to apply criteria sentences")
	private CriteriaOperatorEnum operator;
	
	@Schema(name = "order", description = "Order to apply criteria sentences")
	private Long order;
	
	@Override
    public int compareTo(CriteriaData o) {
        return this.getOrder().compareTo(o.getOrder());
    }
	
}

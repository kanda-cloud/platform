//package cloud.kanda.platform.utils.data.deserialize;
//
//import java.io.IOException;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.ObjectCodec;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cloud.kanda.platform.utils.data.DataContextPageable;
//import cloud.kanda.platform.utils.data.pagination.CustomPageRequest;
//
//public class DataContextPageableDeserialize extends DataContextDeserialize {
//
//	public DataContextPageable deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
//		DataContextPageable dataContext = (DataContextPageable) super.deserialize(jp, dc);
//		ObjectCodec oc = jp.getCodec();
//		JsonNode node = oc.readTree(jp);
//		ObjectMapper om = new ObjectMapper();
//				
//		// Así ignorará las propiedades que no encuentre en el objeto
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		
//		if (node.get("pageable") != null) {
//			dataContext.setPageable(om.treeToValue(node.get("pageable"), CustomPageRequest.class));
//		}
//		
//		return dataContext;
//	}
//
//}

package cloud.kanda.platform.utils.data.deserialize;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PageableGsonDeserialize implements JsonDeserializer<Pageable> {
	
	@Override
	public Pageable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
		JsonObject pageRequestJson = json.getAsJsonObject();
		
		Integer page = null;
		Integer size = null;
		
		if (pageRequestJson.get("page") != null && !pageRequestJson.get("page").isJsonNull()) {
			page = pageRequestJson.get("page").getAsInt();
		}
		
		if (pageRequestJson.get("size") != null && !pageRequestJson.get("size").isJsonNull()) {
			size = pageRequestJson.get("size").getAsInt();
		}

		if (pageRequestJson.get("sort") != null && !pageRequestJson.get("sort").isJsonNull()) {
			pageRequestJson.get("sort");
		}
		
		List<Order> orders = new ArrayList<>();
		Sort sort = Sort.by(orders);
		
		if (page != null && size != null && sort != null) {
			return PageRequest.of(page, size, sort);
		}
		
		return null;
	}
    
}

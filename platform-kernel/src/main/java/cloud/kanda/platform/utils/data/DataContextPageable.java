package cloud.kanda.platform.utils.data;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;

import com.google.gson.annotations.JsonAdapter;

import cloud.kanda.platform.utils.data.deserialize.DataContextPageableGsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

@FieldNameConstants
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonAdapter(DataContextPageableGsonDeserialize.class)
public class DataContextPageable extends DataContext {
	
	private PageRequest pageable;
	
	public DataContextPageable(DataContext dataContext) {
		this.setCriteriaContent(dataContext.getCriteriaContent());
		this.setObject(dataContext.getObject());
		this.setResult(dataContext.getResult());
	}

}

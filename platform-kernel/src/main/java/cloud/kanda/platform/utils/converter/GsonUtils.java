package cloud.kanda.platform.utils.converter;

import cloud.kanda.platform.entity.BaseEntity;
import cloud.kanda.platform.utils.data.deserialize.DateGsonDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import lombok.experimental.UtilityClass;

import java.util.Date;

@UtilityClass
public class GsonUtils {

    public Gson getGson() {
        return getGson(Boolean.FALSE);
    }

    /**
     * Generar un conversor Gson personalizado para java.util.Date y el ObjectId de mongodb recibido como _id se asigna al campo id en String
     */
    public Gson getGson(final Boolean convertId) {
        final GsonBuilder gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateGsonDeserializer())
                .registerTypeAdapter(String.class, (JsonDeserializer<String>) (json, typeOfT, context) ->
                        json.isJsonObject() && json.getAsJsonObject().get("$oid") != null ?
                                json.getAsJsonObject().get("$oid").getAsString() :
                                json.getAsString());

        if (convertId) {
            gson.setFieldNamingStrategy(field -> field.getName().equals(BaseEntity.Fields.id) ? BaseEntity.INTERNAL_ID : field.getName());
        }

        return gson.create();
    }

    public <T, S> T cloneObject(final S object, final Class<T> clazz) {
        final Gson gson = GsonUtils.getGson();
        return gson.fromJson(gson.toJson(object), clazz);
    }

}

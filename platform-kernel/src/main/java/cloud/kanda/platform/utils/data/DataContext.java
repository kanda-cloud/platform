package cloud.kanda.platform.utils.data;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;

import com.google.gson.annotations.JsonAdapter;

import cloud.kanda.platform.utils.data.deserialize.DataContextGsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

@Schema(name = "DataContext", description = "Information to apply custom CRUD")
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@JsonAdapter(DataContextGsonDeserialize.class)
public class DataContext {

	@Schema(name = "clazzObject", description = "Class to deserialize object value")
	private String clazzObject;
	
	@Schema(name = "object", description = "Object value for apply to filter")
	private Object object;
	
	@Builder.Default
	@Schema(name = "criteriaContent", description = "Specify filters criteria of object")
	private CriteriaContent criteriaContent = new CriteriaContent();

	@Schema(name = "result", description = "Value result of filter applied")
	private Object result;
	
	@Schema(hidden = true)
	public Class<?> getObjectClass() {
		return object.getClass();
	}
	
	@Schema(hidden = true)
	public Map<Long, Map<Long, Criteria>> getLevelCriteriaPreBuilded() {
		return criteriaContent.getLevelCriteriaPreBuilded();
	}
	
	@Schema(hidden = true)
	public Map<Long, List<CriteriaData>> getLevelCriteriaData() {
		return criteriaContent.getLevelCriteriaData();
	}
	
	@Schema(hidden = true)
	public Map<Long, List<CriteriaCombination>> getLevelCriteriaOrderCombination() {
		return criteriaContent.getLevelCriteriaOrderCombination();
	}
	
	@Schema(hidden = true)
	public List<CriteriaCombination> getLevelCriteriaCombination() {
		return criteriaContent.getLevelCriteriaCombination();
	}
	
	@Schema(hidden = true)
	public void cleanCriteriaData() {
		criteriaContent = new CriteriaContent();
	}
	
}

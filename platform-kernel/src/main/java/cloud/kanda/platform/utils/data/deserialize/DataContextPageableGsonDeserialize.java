package cloud.kanda.platform.utils.data.deserialize;

import java.lang.reflect.Type;

import org.springframework.data.domain.PageRequest;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;

public class DataContextPageableGsonDeserialize extends DataContextGsonDeserialize {

	@Override
	public DataContext deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		DataContextPageable dataContext = new DataContextPageable(super.deserialize(json, typeOfT, context));
		Gson gson = new Gson();

		JsonObject dataContextJson = json.getAsJsonObject();
				
		if (dataContextJson.get(DataContextPageable.Fields.pageable) != null) {
			dataContext.setPageable(gson.fromJson(dataContextJson.get(DataContextPageable.Fields.pageable), PageRequest.class));
		}
		
		return dataContext;
	}

}

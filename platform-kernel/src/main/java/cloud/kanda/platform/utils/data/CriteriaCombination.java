package cloud.kanda.platform.utils.data;

import com.google.gson.annotations.JsonAdapter;

import cloud.kanda.platform.repository.imp.CriteriaOperatorEnum;
import cloud.kanda.platform.utils.data.deserialize.CriteriaCombinationGsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

@Schema(name = "CriteriaCombination", description = "Information to combinate two differents combinations or two differents orders")
@FieldNameConstants
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonAdapter(CriteriaCombinationGsonDeserialize.class)
public class CriteriaCombination {

	@Schema(name = "first", description = "Order value of first combination")
	private Long first;
	@Schema(name = "firstCombination", description = "Combination value of first combination")
	private CriteriaCombination firstCombination;
	@Schema(name = "second", description = "Order value of second combination")
	private Long second;
	@Schema(name = "secondCombination", description = "Combination value of second combination")
	private CriteriaCombination secondCombination;
	@Schema(name = "operator", description = "Operator to apply to combine the first and second values")
	private CriteriaOperatorEnum operator;

}

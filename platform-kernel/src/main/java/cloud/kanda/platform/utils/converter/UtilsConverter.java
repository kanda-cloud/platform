package cloud.kanda.platform.utils.converter;

import com.google.gson.Gson;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class UtilsConverter {

    public <O> O convertToObject(final Object object, final Class<O> clazz) {
        final Gson gson = GsonUtils.getGson();
        final String json = gson.toJson(object);
        return gson.fromJson(json, clazz);
    }

    @SuppressWarnings("unchecked")
    public <O> List<O> convertListUntypedToListTyped(final Object object, final Class<O> clazz) {
        final Gson gson = GsonUtils.getGson();
        final List<O> list = (List<O>) object;
        return list.stream().map(o -> gson.fromJson(gson.toJsonTree(o), clazz)).collect(Collectors.toList());
    }

    public <O> O convertToObject(final Object object, final Type type) {
        final Gson gson = GsonUtils.getGson();
        final String json = gson.toJson(object);
        return gson.fromJson(json, type);
    }

}

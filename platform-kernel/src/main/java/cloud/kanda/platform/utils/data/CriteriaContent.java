package cloud.kanda.platform.utils.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.mongodb.core.query.Criteria;

import cloud.kanda.platform.repository.imp.CriteriaOperatorEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Schema(name = "CriteriaContent", description = "Information to specify a filter with criteria of entity for apply custom CRUD")
@Getter
@Setter
public class CriteriaContent {

	@Schema(name = "levelCriteriaPreBuilded", description = "")
	private Map<Long, Map<Long, Criteria>> levelCriteriaPreBuilded = new HashMap<>();
	
	@Schema(name = "levelCriteriaData", description = "")
	private Map<Long, List<CriteriaData>> levelCriteriaData = new HashMap<>();
	
	@Schema(name = "levelCriteriaOrderCombination", description = "")
	private Map<Long, List<CriteriaCombination>> levelCriteriaOrderCombination = new HashMap<>();
	
	@Schema(name = "levelCriteriaCombination", description = "")
	private List<CriteriaCombination> levelCriteriaCombination = new ArrayList<>();
	
	@Schema(hidden = true)
	public Set<Long> getLevels() {
		return this.levelCriteriaData.keySet();
	}
	
	@Schema(hidden = true)
	public void generateDefaultCriteriaObject(Object object) {
		CriteriaData criteriaData = UtilsCriteria.generateCriteriaData(0L, CriteriaOperatorEnum.AND, UtilsCriteria.getSentencesDefault(object));
		List<CriteriaData> listCriteriaData = new ArrayList<>();
		listCriteriaData.add(criteriaData);
		this.levelCriteriaData.put(0L, listCriteriaData);
	}
	
	@Schema(hidden = true)
	public void excludeAudit() {
		CriteriaData criteriaData = UtilsCriteria.generateCriteriaData(-1L, CriteriaOperatorEnum.AND, UtilsCriteria.getSentencesExcludeAudit());
		UtilsCriteria.addCriteriaDataLevelImplicit(this.levelCriteriaData, criteriaData);
    }
	
}

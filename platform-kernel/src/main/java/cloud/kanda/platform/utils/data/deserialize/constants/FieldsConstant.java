package cloud.kanda.platform.utils.data.deserialize.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class FieldsConstant {
		
	public final String _CLASS = "_class";
	
}

package cloud.kanda.platform.utils.data.deserialize;

import cloud.kanda.platform.utils.converter.GsonUtils;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.stream.Collectors;

public final class MapGsonDeserializer implements JsonDeserializer<Map<String, String>>, JsonSerializer<Map<String, String>> {

    @Override
    public Map<String, String> deserialize(final JsonElement json,
                                           final Type type,
                                           final JsonDeserializationContext jdc) throws JsonParseException {
        if (json.isJsonObject()) {
            return json.getAsJsonObject().entrySet().stream()
                    .filter(e -> !JsonNull.class.isAssignableFrom(e.getValue().getClass()))
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getAsString()));
        }

        return null;
    }

    @Override
    public JsonElement serialize(final Map<String, String> map,
                                 final Type type,
                                 final JsonSerializationContext jdc) throws JsonParseException {
        final Gson gson = GsonUtils.getGson();
        final JsonObject json = new JsonObject();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            json.add(entry.getKey(), gson.toJsonTree(entry.getValue()));
        }

        return json;
    }

}
package cloud.kanda.platform.utils.data.deserialize;

import cloud.kanda.platform.utils.converter.GsonUtils;
import cloud.kanda.platform.utils.data.CriteriaContent;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.deserialize.constants.FieldsConstant;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DataContextGsonDeserialize implements JsonDeserializer<DataContext> {

    @Override
    public DataContext deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {
        final DataContext dataContext = new DataContext();
        final Gson gson = GsonUtils.getGson();

        final JsonObject dataContextJson = json.getAsJsonObject();

        if (dataContextJson.get(DataContext.Fields.criteriaContent) != null && !dataContextJson.get(DataContext.Fields.criteriaContent).isJsonNull()) {
            dataContext.setCriteriaContent(gson.fromJson(dataContextJson.get(DataContext.Fields.criteriaContent), CriteriaContent.class));
        } else {
            dataContext.setCriteriaContent(new CriteriaContent());
        }

        String clazzObjectTemp = null;
        if (dataContextJson.get(DataContext.Fields.clazzObject) != null && !dataContextJson.get(DataContext.Fields.clazzObject).isJsonNull()) {
            clazzObjectTemp = dataContextJson.get(DataContext.Fields.clazzObject).getAsString();
        }
        final String clazzObject = clazzObjectTemp;

        if (dataContextJson.get(DataContext.Fields.object) != null) {
            if (dataContextJson.get(DataContext.Fields.object).isJsonArray()) {
                final JsonArray objectsJson = dataContextJson.get(DataContext.Fields.object).getAsJsonArray();
                final List<Object> listObject = new ArrayList<>();
                objectsJson.forEach(item -> {
                    final JsonObject objectJson = (JsonObject) item;
                    listObject.add(this.parseToClass(gson, objectJson, clazzObject));
                });
                dataContext.setObject(listObject);

            } else {
                final JsonObject objectJson = dataContextJson.get(DataContext.Fields.object).getAsJsonObject();
                dataContext.setObject(this.parseToClass(gson, objectJson, clazzObject));
            }
        }

        if (dataContextJson.get(DataContext.Fields.result) != null && !dataContextJson.get(DataContext.Fields.result).isJsonNull()) {
            if (dataContextJson.get(DataContext.Fields.result).isJsonArray()) {
                final JsonArray objectsJson = dataContextJson.get(DataContext.Fields.result).getAsJsonArray();
                final List<Object> listObject = new ArrayList<>();
                objectsJson.forEach(item -> {
                    final JsonObject objectJson = (JsonObject) item;
                    listObject.add(this.parseToClass(gson, objectJson, clazzObject));
                });
                dataContext.setResult(listObject);
            } else if (dataContextJson.get(DataContext.Fields.result).isJsonObject()) {
                final JsonObject objectJson = dataContextJson.get(DataContext.Fields.result).getAsJsonObject();
                dataContext.setResult(this.parseToClass(gson, objectJson, clazzObject));
            } else if (dataContextJson.get(DataContext.Fields.result).isJsonPrimitive()) {
                final JsonPrimitive objectJson = dataContextJson.get(DataContext.Fields.result).getAsJsonPrimitive();
                dataContext.setResult(objectJson.getAsString());
            }
        }

        return dataContext;
    }

    private Object parseToClass(final Gson gson, final JsonObject objectJson, final String clazzObject) {
        Object obj = null;
        String classToParse = null;
        if (objectJson.get(FieldsConstant._CLASS) != null) {
            classToParse = objectJson.get(FieldsConstant._CLASS).getAsString();
        } else if (clazzObject != null) {
            classToParse = clazzObject;
        }

        if (classToParse != null) {
            try {
                obj = gson.fromJson(objectJson, Class.forName(classToParse));
            } catch (final ClassNotFoundException e) {
                log.error(e.getMessage(), e);
            }
        }

        return obj;
    }

}

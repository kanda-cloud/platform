//package cloud.kanda.platform.utils.data.deserialize;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.ObjectCodec;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.JsonDeserializer;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cloud.kanda.platform.utils.data.DataContext;
//
//@SuppressWarnings("unchecked")
//public class DataContextDeserialize extends JsonDeserializer<DataContext> {
//
//	@Override
//	public DataContext deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
//		ObjectCodec oc = jp.getCodec();
//		JsonNode node = oc.readTree(jp);
//		DataContext dataContext = new DataContext();
//		ObjectMapper om = new ObjectMapper();
//		Map<String, Object> ob = new HashMap<>();
//		List<Map<String, Object>> el = new ArrayList<>();
//		
//		// Así ignorará las propiedades que no encuentre en el objeto
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		
//		if (node.get("criteria") != null) {
//			dataContext.setCriteria(om.treeToValue(node.get("criteria"), HashMap.class));
//		} else {
//			dataContext.setCriteria(new HashMap<>());
//		}
//		
//		if (node.get("object") != null) {
//			
//			Iterator<JsonNode> elements = node.get("object").elements();
//			Iterator<Entry<String, JsonNode>> nodes = node.get("object").fields();
//			JsonNode nodeClass = node.get("object").get("_class");
//			Boolean multiple = Boolean.FALSE;
//			
//			if (nodes.hasNext()) {
//				while (nodes.hasNext()) {
//					try {
//						Entry<String, JsonNode> n = nodes.next();
//						String className = null;
//						JsonNode nodeTemp = null;
//						
//						// Si viene solo un objeto
//						if (nodeClass != null) {
//							className = nodeClass.textValue();
//							nodeTemp = node.get("object");
//						} // Si vienen varios objetos
//						else if (n.getValue().get("_class") != null) {
//							className = n.getValue().get("_class").textValue();
//							nodeTemp = node.get("object").get(n.getKey());
//							multiple = Boolean.TRUE;
//						} 
//						else {
//							Map<String, Object> objects = new HashMap<>();
//							objects.put(n.getKey(), om.treeToValue(n.getValue(), Object.class));
//							el.add(objects);
//							dataContext.setObject(el);
//							break;
//						}
//						
//						// Mapear el objeto a su clase y añadirlo al campo object para setearlo a datacontext 
//						// Si es múltiple se van añadiendo los objetos a un map
//						// Si no es múltiple se añade directamente al object de datacontext
//						if (multiple) {
//							ob.put(n.getKey(), om.treeToValue(nodeTemp, Class.forName(className)));
//							className = null;
//						} else if (nodeTemp != null) {
//							dataContext.setObject(om.convertValue(nodeTemp, Class.forName(className)));
//							break;
//						}
//
//					} catch (ClassNotFoundException e) {
//						e.printStackTrace();
//					}
//				}
//				
//				// Reasigna el map de object si es múltiple, si no el objeto ya ha sido asigando anteriormente
//				if (multiple) {
//					dataContext.setObject(ob);
//				}
//				
//			} else if (elements.hasNext()) {
//				while (elements.hasNext()) {
//					Map<String, Object> objects = new HashMap<>();
//					JsonNode jn = elements.next();
//					if (jn.fields().hasNext()) {
//						Entry<String, JsonNode> subElements = jn.fields().next();
//						objects.put(subElements.getKey(), om.treeToValue(subElements.getValue(), Object.class));
//						el.add(objects);
//					}
//				}
//				dataContext.setObject(el);
//			}
//		}
//		
//		return dataContext;
//	}
//
//}

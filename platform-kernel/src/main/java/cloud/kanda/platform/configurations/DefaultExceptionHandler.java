package cloud.kanda.platform.configurations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Locale;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {
	
	/**
	 * Este método es el encargado de la gestión de todas las excepciones, se encarga de capturar las excepciones del tipo
	 * Throwable.class, Exception.class, NullPointerException.class, AccessDeniedException.class, ConnectException.class, IOException.class.
	 * Una vez analizada la excepción genera un mensaje de error según el tipo y lo devuelve en un modelo a la web para ser mostrado al usuario.
	 *
	 * @param exception the exception
	 * @param principal the principal
	 * @param locale the locale
	 * @param resp the resp
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler({ Throwable.class, Exception.class, NullPointerException.class, ConnectException.class, IOException.class, SQLException.class, NoRouteToHostException.class, ServletException.class })
	public ResponseEntity<Object> exception(Exception exception, Principal principal, Locale locale, HttpServletResponse resp, HttpServletRequest request){
		log.error(exception.getMessage(), exception);
		
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}

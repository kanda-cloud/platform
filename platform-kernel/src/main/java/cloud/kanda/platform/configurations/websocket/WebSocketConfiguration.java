package cloud.kanda.platform.configurations.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

	@Value("${server.servlet.context-path}")
	private String contextPath;

	@Autowired
	private IEndpointsWSConfig endpointsWSConfig;

	@Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        registry.addEndpoint(this.endpointsWSConfig.stompEndpoints())
                .setAllowedOriginPatterns(this.endpointsWSConfig.getCorsOrigin());
    }

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes(this.contextPath)
                .enableSimpleBroker(this.endpointsWSConfig.getEnableSimpleBroker());
    }

}

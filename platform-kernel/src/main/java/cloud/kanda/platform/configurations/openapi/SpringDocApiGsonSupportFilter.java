package cloud.kanda.platform.configurations.openapi;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

@Configuration
public class SpringDocApiGsonSupportFilter extends OncePerRequestFilter {

    private static final String V3_API_DOCS = "/v3/api-docs";
    private static final String V3_API_DOCS_SWAGGER_CONFIG = "/v3/api-docs/swagger-config";

    @Override
    public void doFilterInternal(final HttpServletRequest request,
                                 final HttpServletResponse response,
                                 final FilterChain filterChain) throws IOException, ServletException {
        final ByteResponseWrapper byteResponseWrapper = new ByteResponseWrapper(response);
        final ByteRequestWrapper byteRequestWrapper = new ByteRequestWrapper(request);

        filterChain.doFilter(byteRequestWrapper, byteResponseWrapper);

        final String jsonResponse = new String(byteResponseWrapper.getBytes(), response.getCharacterEncoding());

        response.getOutputStream()
                .write(jsonResponse.getBytes(response.getCharacterEncoding()));
    }

    @Override
    protected boolean shouldNotFilter(final HttpServletRequest request) {
        final String path = request.getServletPath();
        return path.equals(V3_API_DOCS_SWAGGER_CONFIG) || !path.startsWith(V3_API_DOCS);
    }

    @Override
    public void destroy() {
    }

    static class ByteResponseWrapper extends HttpServletResponseWrapper {

        private final PrintWriter writer;
        private final ByteOutputStream output;

        public byte[] getBytes() {
            this.writer.flush();
            return this.output.getBytes();
        }

        public ByteResponseWrapper(final HttpServletResponse response) {
            super(response);
            this.output = new ByteOutputStream();
            this.writer = new PrintWriter(this.output);
        }

        @Override
        public PrintWriter getWriter() {
            return this.writer;
        }

        @Override
        public ServletOutputStream getOutputStream() {
            return this.output;
        }
    }

    static class ByteRequestWrapper extends HttpServletRequestWrapper {

        private byte[] requestBytes = null;
        private ByteInputStream byteInputStream;


        public ByteRequestWrapper(final HttpServletRequest request) throws IOException {
            super(request);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();

            final InputStream inputStream = request.getInputStream();

            final byte[] buffer = new byte[4096];
            int read = 0;
            while ((read = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, read);
            }

            this.replaceRequestPayload(baos.toByteArray());
        }

        @Override
        public BufferedReader getReader() {
            return new BufferedReader(new InputStreamReader(this.getInputStream()));
        }

        @Override
        public ServletInputStream getInputStream() {
            return this.byteInputStream;
        }

        public void replaceRequestPayload(final byte[] newPayload) {
            this.requestBytes = newPayload;
            this.byteInputStream = new ByteInputStream(new ByteArrayInputStream(this.requestBytes));
        }
    }

    static class ByteOutputStream extends ServletOutputStream {

        private final ByteArrayOutputStream bos = new ByteArrayOutputStream();

        @Override
        public void write(final int b) {
            this.bos.write(b);
        }

        public byte[] getBytes() {
            return this.bos.toByteArray();
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(final WriteListener writeListener) {

        }
    }

    static class ByteInputStream extends ServletInputStream {

        private final InputStream inputStream;

        public ByteInputStream(final InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public int read() throws IOException {
            return this.inputStream.read();
        }

        @Override
        public boolean isFinished() {
            return false;
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setReadListener(final ReadListener readListener) {

        }
    }
}
package cloud.kanda.platform.configurations;

import java.util.Arrays;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cloud.kanda.platform.utils.data.CriteriaCombination;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import cloud.kanda.platform.utils.data.deserialize.CriteriaCombinationGsonDeserialize;
import cloud.kanda.platform.utils.data.deserialize.DataContextGsonDeserialize;
import cloud.kanda.platform.utils.data.deserialize.DataContextPageableGsonDeserialize;
import cloud.kanda.platform.utils.data.deserialize.DateGsonDeserializer;
import cloud.kanda.platform.utils.data.deserialize.DateTimeGsonDeserializer;
import cloud.kanda.platform.utils.data.deserialize.PageableGsonDeserialize;

@Configuration
@ConditionalOnClass(Gson.class)
public class GsonConfiguration {

    @Bean
    public GsonHttpMessageConverter gsonHttpMessageConverter() {
    	GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();
        gsonHttpMessageConverter.setGson(this.gson());
        gsonHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        
        return gsonHttpMessageConverter;
    }

    @Bean
    public Gson gson() {
		return new GsonBuilder()
  			.registerTypeAdapter(DateTime.class, new DateTimeGsonDeserializer())
  			.registerTypeAdapter(Date.class, new DateGsonDeserializer())
            .registerTypeAdapter(CriteriaCombination.class, new CriteriaCombinationGsonDeserialize())
            .registerTypeAdapter(DataContext.class, new DataContextGsonDeserialize())
      		.registerTypeAdapter(DataContextPageable.class, new DataContextPageableGsonDeserialize())
            .registerTypeAdapter(Pageable.class, new PageableGsonDeserialize())
            .serializeNulls()
            .create();
    }
    
}
package cloud.kanda.platform.configurations.websocket;

public interface IEndpointsWSConfig {

    Boolean SAME_ORIGIN = Boolean.TRUE;

    String[] ENABLE_SIMPLE_BROKER_DEFAULT = {
            "/get-status",
            "/get-maintenance"
    };

    String[] SIMP_SUBSCRIBE_DEST_MATCHERS_DEFAULT = {
            "/get-status"
    };

    String[] SIMP_DEST_MATCHERS_AUTHENTICATED_DEFAULT = {};

    String[] SIMP_DEST_MATCHERS_PERMIT_ALL_DEFAULT = {};

    String[] STOMP_ENDPOINTS_DEFAULT = {
            "/socket",
            "/socket-auth"
    };

    String getCorsOrigin();

    default String[] getEnableSimpleBroker() {
        return ENABLE_SIMPLE_BROKER_DEFAULT;
    }

    default String[] getSimpSubscribeDestMatchers() {
        return SIMP_SUBSCRIBE_DEST_MATCHERS_DEFAULT;
    }

    default String[] getSimpDestMatchersAuthenticated() {
        return SIMP_DEST_MATCHERS_AUTHENTICATED_DEFAULT;
    }

    default String[] getSimpDestMatchersPermitAll() {
        return SIMP_DEST_MATCHERS_PERMIT_ALL_DEFAULT;
    }

    default Boolean sameOriginDisabled() {
        return SAME_ORIGIN;
    }

    default String[] stompEndpoints() {
        return STOMP_ENDPOINTS_DEFAULT;
    }

}

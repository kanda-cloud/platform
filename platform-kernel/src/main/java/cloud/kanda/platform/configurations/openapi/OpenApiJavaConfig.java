//package cloud.kanda.platform.configurations.openapi;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import io.swagger.v3.oas.models.Components;
//import io.swagger.v3.oas.models.OpenAPI;
//import io.swagger.v3.oas.models.info.Contact;
//import io.swagger.v3.oas.models.info.Info;
//import io.swagger.v3.oas.models.info.License;
//import io.swagger.v3.oas.models.security.OAuthFlow;
//import io.swagger.v3.oas.models.security.OAuthFlows;
//import io.swagger.v3.oas.models.security.Scopes;
//import io.swagger.v3.oas.models.security.SecurityScheme;
//
//@Configuration
//public class OpenApiJavaConfig  {
//
//	@Value("${path.keycloak}")
//	private String PATH_KEYCLOAK;
//
//	@Value("${build.version}")
//	private String BUILD_VERSION;
//	
//	@Value("${openapi.description}")
//	private String OPENAPI_DESCRIPTION;
//	
//	@Value("${openapi.title}")
//	private String OPENAPI_TITLE;
//	
//	@Value("${openapi.license.name}")
//	private String OPENAPI_LICENSE_NAME;
//	
//	@Value("${openapi.license.url}")
//	private String OPENAPI_LICENSE_URL;
//
//	@Value("${openapi.contact.email}")
//	private String OPENAPI_CONTACT_EMAIL;
//
//	@Bean
//	public OpenAPI api() {
//		return new OpenAPI().info(this.apiInfo())
//							.components(this.getComponents());
//	}
//
//	private Info apiInfo() {
//		return new Info().title(this.OPENAPI_TITLE)
//						 .description(this.OPENAPI_DESCRIPTION)
//						 .version(this.BUILD_VERSION)
//						 .license(this.getLicense())
//						 .contact(this.getContact());
//	}
//
//	private Contact getContact() {
//		return new Contact().email(this.OPENAPI_CONTACT_EMAIL);
//	}
//
//	private License getLicense() {
//		return new License().name(this.OPENAPI_LICENSE_NAME).url(this.OPENAPI_LICENSE_URL);
//	}
//
//    private Components getComponents() {
//    	return new Components().addSecuritySchemes(OpenApiConfig.KEYCLOAK_OAUTH, this.getSecuritySchema());
//    }
//    
//    private SecurityScheme getSecuritySchema() {
//    	return new SecurityScheme().name(OpenApiConfig.KEYCLOAK_OAUTH)
//    							   .type(SecurityScheme.Type.OAUTH2)
//    							   .flows(this.getOAuthFlows());
//    }
//    
//    private OAuthFlows getOAuthFlows() {
//    	return new OAuthFlows().implicit(new OAuthFlow().authorizationUrl(this.PATH_KEYCLOAK + "/auth")
//		   												.refreshUrl(this.PATH_KEYCLOAK + "/token")
//		   												.tokenUrl(this.PATH_KEYCLOAK + "/token")
//		   												.scopes(new Scopes()));
//    }
//	
//}
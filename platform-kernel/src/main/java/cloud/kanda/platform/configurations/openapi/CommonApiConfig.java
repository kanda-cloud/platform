package cloud.kanda.platform.configurations.openapi;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;

//INFO: desde Kloud se auto agrupan por microservicios
@Deprecated
//@Configuration
public class CommonApiConfig {
	
	@Bean
	public GroupedOpenApi commonsApiOpenApi() {
	   final String[] paths = {"/api/**"};
	   return GroupedOpenApi.builder()
			   				.group("Api Commons")
			   				.pathsToMatch(paths)
			   				.build();
	}
	
}

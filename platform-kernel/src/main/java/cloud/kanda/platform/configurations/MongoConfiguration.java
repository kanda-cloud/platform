package cloud.kanda.platform.configurations;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ConditionalOnProperty(value = "scan.packages.mongo.repository")
@EnableMongoAuditing(modifyOnCreate = false)
@EnableMongoRepositories(basePackages = {"${scan.packages.mongo.repository}"})
@ComponentScan({"${scan.packages.mongo.entity}"})
@EnableTransactionManagement
public class MongoConfiguration extends AbstractMongoClientConfiguration {

    @Value("${mongo.protocol}")
    private String mongoProtocol;

    @Value("${mongo.user}")
    private String mongoUser;

    @Value("${mongo.password}")
    private String mongoPassword;

    @Value("${mongo.url}")
    private String mongoUrl;

    @Value("${mongo.atlas}")
    private String mongoAtlas;

    @Value("${mongo.bd}")
    private String mongoBd;

    @Bean
    public MongoTransactionManager transactionManager(final MongoDatabaseFactory dbFactory) {
        return new MongoTransactionManager(dbFactory);
    }

    @Bean
    public GridFsTemplate gridFsTemplate(final MappingMongoConverter mappingMongoConverter) throws Exception {
        return new GridFsTemplate(this.mongoDbFactory(), mappingMongoConverter);
    }

    @Bean
    public CascadingMongoEventListener cascadingMongoEventListener() {
        return new CascadingMongoEventListener();
    }

    @Override
    protected String getDatabaseName() {
        return this.mongoBd;
    }

    private String getUrlMongoDB() {
        final String url = this.mongoProtocol.concat("://")
                .concat(this.mongoUser)
                .concat(":")
                .concat(this.mongoPassword)
                .concat("@")
                .concat(this.mongoUrl);

        if (Boolean.parseBoolean(this.mongoAtlas)) {
            return url;
        } else {
            return url.concat("/")
                    .concat(this.mongoBd);
        }

    }

    @Override
    public MongoClient mongoClient() {
        return MongoClients.create(new ConnectionString(this.getUrlMongoDB()));
    }

}

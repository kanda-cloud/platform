package cloud.kanda.platform.configurations.openapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecuritySchemes;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
	info = @Info(
		title = "${openapi.title}", 
		description = "${openapi.description}",
		version = "${build.version}",
		license = @License(
			name = "${openapi.license.name}", 
			url = "${openapi.license.url}"
		), 
		contact = @Contact(
			email = "${openapi.contact.email}"
		)
	)
)

@SecuritySchemes({
	@SecurityScheme(
		name = OpenApiConfig.KEYCLOAK_OAUTH, 
		type = SecuritySchemeType.OAUTH2, 
		flows = @OAuthFlows(
			implicit = @OAuthFlow(
				authorizationUrl = "${path.keycloak}/auth",
				refreshUrl = "${path.keycloak}/token", 
				tokenUrl = "${path.keycloak}/token"
			)
		)
	) 
})
public class OpenApiConfig {
	public static final String KEYCLOAK_OAUTH = "keycloakOauth";
}
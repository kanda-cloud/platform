package cloud.kanda.platform.configurations;

import cloud.kanda.platform.annotation.CascadeDelete;
import cloud.kanda.platform.annotation.CascadePurgue;
import cloud.kanda.platform.annotation.CascadeSave;
import cloud.kanda.platform.entity.BaseEntity;
import cloud.kanda.platform.entity.EntityDocument;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;

/**
 * The listener interface for receiving cascadingMongoEvent events.
 * The class that is interested in processing a cascadingMongoEvent
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addCascadingMongoEventListener<code> method. When
 * the cascadingMongoEvent event occurs, that object's appropriate
 * method is invoked.
 */
@SuppressWarnings({"unchecked", "unused"})
public class CascadingMongoEventListener extends AbstractMongoEventListener<Object> {

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public void onBeforeDelete(@NonNull final BeforeDeleteEvent<Object> source) {
        final Class<?> clazz = source.getType();

        if (clazz != null && source.getDocument() != null) {
            final String id = source.getDocument().get(BaseEntity.INTERNAL_ID).toString();
            if (clazz.isAnnotationPresent(CascadeDelete.class)) {
                final CascadeDelete cascadeDelete = clazz.getAnnotation(CascadeDelete.class);
                for (final Class<? extends EntityDocument<String>> clazzEntity : cascadeDelete.classes()) {
                    this.deleteReferenceEntity(clazz, clazzEntity, id);
                }
            } else if (clazz.isAnnotationPresent(CascadePurgue.class)) {
                final CascadePurgue cascadePurgue = clazz.getAnnotation(CascadePurgue.class);
                for (final Class<? extends EntityDocument<String>> clazzEntity : cascadePurgue.classes()) {
                    this.purgueReferenceEntity(clazz, clazzEntity, id, source);
                }
            }
        }
    }

    private void deleteReferenceEntity(final Class<?> clazz,
                                       final Class<? extends EntityDocument<String>> clazzEntity,
                                       final String id) {
        ReflectionUtils.doWithFields(clazzEntity, field -> {
            ReflectionUtils.makeAccessible(field);

            if (field.getType().equals(clazz)) {
                final List<?> entities = this.findReferencedEntities(id, clazzEntity, field);
                for (final Object entity : entities) {
                    this.mongoOperations.remove(entity);
                }
            } else if (field.getType().equals(List.class)) {
                final ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
                final Class<?> stringListClass = (Class<?>) stringListType.getActualTypeArguments()[0];

                if (stringListClass.equals(clazz)) {
                    final List<?> entities = this.findReferencedEntities(id, clazzEntity, field);
                    final List<String> referencedIds = entities.stream().map(BaseEntity.class::cast).map(BaseEntity::getId).toList();
                    final Query query = new Query(Criteria.where(BaseEntity.Fields.id).in(referencedIds));
                    this.mongoOperations.remove(query, clazzEntity);
                }
            }
        });
    }

    private void purgueReferenceEntity(final Class<?> clazz,
                                       final Class<? extends EntityDocument<String>> clazzEntity,
                                       final String id,
                                       final BeforeDeleteEvent<Object> source) {
        ReflectionUtils.doWithFields(clazzEntity, field -> {
            ReflectionUtils.makeAccessible(field);

            if (field.getType().equals(clazz)) {
                final List<?> entities = this.findReferencedEntities(id, clazzEntity, field);
                for (final Object entity : entities) {
                    setValue(field, entity, null);
                    this.mongoOperations.save(entity);
                }
            } else if (field.getType().equals(List.class)) {
                final ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
                final Class<?> stringListClass = (Class<?>) stringListType.getActualTypeArguments()[0];

                if (stringListClass.equals(clazz)) {
                    final List<?> entities = this.findReferencedEntities(id, clazzEntity, field);

                    this.purgueList(id, source, field, entities);
                }
            }
        });
    }

    private void purgueList(final String id,
                            final BeforeDeleteEvent<Object> source,
                            final Field field,
                            final List<?> entities) throws IllegalAccessException {
        for (final Object entity : entities) {
            final List<?> collection = (List<?>) field.get(entity);
            if (!collection.isEmpty()) {
                for (final Object item : collection) {
                    final BaseEntity baseEntity = (BaseEntity) item;
                    if (baseEntity.getId().equals(id)) {
                        collection.remove(item);
                        break;
                    }
                }
                collection.remove(source.getSource());
            }

            this.mongoOperations.save(entity);
        }
    }

    private List<?> findReferencedEntities(final String id, final Class<? extends EntityDocument<String>> clazzEntity, final Field field) {
        final Query query = new Query(Criteria.where(field.getName()).is(id));

        return this.mongoOperations.find(query, clazzEntity);
    }


    @Override
    public void onBeforeConvert(final BeforeConvertEvent<Object> source) {
        ReflectionUtils.doWithFields(source.getSource().getClass(), field -> {
            ReflectionUtils.makeAccessible(field);
            if (field.isAnnotationPresent(Id.class) && field.get(source.getSource()) == null) {
                setValue(field, source.getSource(), new ObjectId().toString());
            }

            if (field.isAnnotationPresent(DBRef.class) && field.isAnnotationPresent(CascadeSave.class)) {
                final Object fieldValue = field.get(source.getSource());
                if (fieldValue != null && Collection.class.isAssignableFrom(fieldValue.getClass())) {
                    final Collection<Object> collection = (Collection<Object>) fieldValue;
                    for (final Object item : collection) {
                        this.mongoOperations.save(item);
                    }
                } else if (fieldValue != null) {
                    this.mongoOperations.save(fieldValue);
                    // Sobreescribir valor por si se le ha asignado un id
                    setValue(field, source.getSource(), fieldValue);
                }
            }
        });
    }

    @SuppressWarnings("java:S3011")
    private static void setValue(final Field field,
                                 final Object source,
                                 final Object value) throws IllegalAccessException {
        field.set(source, value);
    }

    private static class DbRefFieldCallback implements ReflectionUtils.FieldCallback {

        private boolean idFound;

        @Override
        public void doWith(@NonNull final Field field) throws IllegalArgumentException {
            ReflectionUtils.makeAccessible(field);

            if (field.isAnnotationPresent(Id.class)) {
                this.idFound = true;
            }
        }

        public boolean isIdFound() {
            return this.idFound;
        }
    }
}

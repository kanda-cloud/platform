//package cloud.kanda.platform.rest.test;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import cloud.kanda.platform.rest.ApiController;
//import cloud.kanda.platform.test.BaseControllerTest;
//
//@WebMvcTest(ApiController.class)
//public class ApiControllerTest extends BaseControllerTest {
//
//	@Autowired
//	protected MockMvc mockMvc;
//
//	public String getPath() {
//		return "/api";
//	}
//
//	@Test
//	@WithMockUser(username = "test", authorities = {"READ"})
//	public void ping() throws Exception {
//		// Given
//
//		// When
//		this.mockMvc.perform(MockMvcRequestBuilders.get(this.getPath().concat("/ping"))
//						.contentType(MediaType.APPLICATION_JSON)
//						.accept(MediaType.APPLICATION_JSON))
//				// Then
//				.andExpect(status().isOk());
//
//	}
//
//}
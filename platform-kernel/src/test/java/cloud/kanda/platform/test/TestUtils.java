package cloud.kanda.platform.test;

import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.deserialize.DataContextGsonDeserialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

import java.util.List;

@UtilityClass
public class TestUtils {

    @SuppressWarnings("rawtypes")
    public List jsonToList(final String json, final TypeToken token) {
        final Gson gson = new Gson();
        return gson.fromJson(json, token.getType());
    }

    public String objectToJson(final Object obj) {
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(DataContext.class, new DataContextGsonDeserialize())
                .create();
        return gson.toJson(obj);
    }

    public <T> T jsonToObject(final String json, final Class<T> classOf) {
        final Gson gson = new Gson();
        return gson.fromJson(json, classOf);
    }

    private EasyRandom getGenerator() {
        final EasyRandomParameters parameters = new EasyRandomParameters()
                .seed(generateLong())
                .stringLengthRange(3, 25)
                .collectionSizeRange(1, 3)
                .scanClasspathForConcreteTypes(Boolean.TRUE)
                .overrideDefaultInitialization(Boolean.FALSE)
                .bypassSetters(Boolean.TRUE);

        return new EasyRandom(parameters);
    }

    public <T> T generateInstance(final Class<T> classInstance) {
        return getGenerator().nextObject(classInstance);
    }

    public String generateString() {
        return generateString(10);
    }

    public String generateString(final int size) {
        return RandomStringUtils.randomAlphabetic(size);
    }

    public Long generateLong() {
        return generateLong(10);
    }

    public Long generateLong(final int size) {
        return Long.valueOf(RandomStringUtils.randomNumeric(size));
    }

}

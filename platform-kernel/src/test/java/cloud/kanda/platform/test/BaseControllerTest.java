package cloud.kanda.platform.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureWebMvc
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ComponentScan({"${scan.packages.mongo.entity}", "${scan.packages.mongo.repository}"})
public abstract class BaseControllerTest {

    @Autowired
    protected MockMvc mockMvc;

}

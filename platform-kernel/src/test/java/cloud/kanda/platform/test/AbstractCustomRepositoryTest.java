package cloud.kanda.platform.test;

import cloud.kanda.platform.entity.BaseEntity;
import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.repository.RepositoryCustom;
import cloud.kanda.platform.utils.data.CriteriaContent;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import cloud.kanda.platform.utils.data.UtilsCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

/**
 * The class AbstractServiceTest.
 *
 * @param <CR> the type custom repository
 * @param <E>  the type entity
 * @param <ID> the type identifier
 */
public abstract class AbstractCustomRepositoryTest<CR extends RepositoryCustom,
        E extends EntityDocument<ID>,
        ID extends Serializable>
        extends AbstractUnitTest {

    // NOT DO GENERIC
    public abstract MongoTemplate getMongoTemplate();

    public abstract Class<E> getClassEntity();

    public abstract CR getCustomRepository();

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
    }

    public E generateEntity() {
        return TestUtils.generateInstance(this.getClassEntity());
    }

    @Test
    public void count() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();
        final long resultExpected = TestUtils.generateLong();

        when(this.getMongoTemplate().count(UtilsCriteria.getQuery(dataContext), this.getClassEntity())).thenReturn(resultExpected);

        // When
        final DataContext result = this.getCustomRepository().count(dataContext);

        // Then
        assertThat(result.getResult()).isEqualTo(resultExpected);
        assertThat(result.getObject()).isEqualTo(dataContext.getObject());
    }

    @Test
    public void exists() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        when(this.getMongoTemplate().exists(UtilsCriteria.getQuery(dataContext), this.getClassEntity())).thenReturn(Boolean.TRUE);

        // When
        final DataContext result = this.getCustomRepository().exists(dataContext);

        // Then
        assertTrue((Boolean) result.getResult());
        assertThat(result.getObject()).isEqualTo(dataContext.getObject());
    }

    @Test
    public void findAll() {

        // Given
        final DataContextPageable dataContext = new DataContextPageable();
        dataContext.setObject(this.generateEntity());
        final List<Order> orders = new ArrayList<>();
        orders.add(Order.by(BaseEntity.Fields.id));
        final PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(orders));
        dataContext.setPageable(pageRequest);
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final E entity1 = this.generateEntity();
        final E entity2 = this.generateEntity();

        final List<E> expectedEntities = new ArrayList<>();
        expectedEntities.add(entity1);
        expectedEntities.add(entity2);

//        when(this.getMongoTemplate().count(UtilsCriteria.getQuery(dataContext), this.getClassEntity())).thenReturn(this.LONG_GENERIC);
        when(this.getMongoTemplate().find(UtilsCriteria.getQuery(dataContext), this.getClassEntity())).thenReturn(expectedEntities);

        // When
        final DataContextPageable result = this.getCustomRepository().findAll(dataContext);

        // Then
        assertThat(result.getResult()).isEqualTo(expectedEntities);
        assertThat(result.getObject()).isEqualTo(dataContext.getObject());

    }

    @Test
    public void findOne() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final E entity = this.generateEntity();

        when(this.getMongoTemplate().findOne(UtilsCriteria.getQuery(dataContext), this.getClassEntity())).thenReturn(entity);

        // When
        final DataContext result = this.getCustomRepository().findOne(dataContext);

        // Then
        assertThat(result.getResult()).isEqualTo(entity);
        assertThat(result.getObject()).isEqualTo(dataContext.getObject());
    }

}

package cloud.kanda.platform.test;

import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.repository.RepositoryCustom;
import cloud.kanda.platform.service.imp.AbstractServiceImp;
import cloud.kanda.platform.utils.converter.GsonUtils;
import cloud.kanda.platform.utils.data.CriteriaContent;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The class AbstractServiceTest.
 *
 * @param <S>  the type getService()
 * @param <R>  the type getRepository()
 * @param <E>  the type entity
 * @param <ID> the type identifier
 */
@SuppressWarnings("unchecked")
public abstract class AbstractServiceTest<S extends AbstractServiceImp<R, E, ID>,
        R extends RepositoryCustom & MongoRepository<E, ID>,
        E extends EntityDocument<ID>,
        ID extends Serializable> extends AbstractUnitTest {

    public abstract Class<E> getClassEntity();

    public abstract R getRepository();

    public abstract S getService();

    public abstract Map<String, Object> getParameters();

    public abstract E internalUpdate(final @NotNull E entity, final @Nullable Object value);

    public E generateEntity() {
        return TestUtils.generateInstance(this.getClassEntity());
    }

    @Test
    public void count() {
        // Given
        final long resultExpected = TestUtils.generateLong();
        when(this.getRepository().count()).thenReturn(resultExpected);

        // When
        final Long result = this.getService().count();

        // Then
        assertNotNull(result);
        assertEquals(result, resultExpected);
    }

    @Test
    public void deleteEntity() {
        // Given
        final E entity = this.generateEntity();
        doNothing().when(this.getRepository()).delete(entity);

        // When
        this.getService().delete(entity);

        // Then
        verify(this.getRepository()).delete(entity);
    }

    @Test
    public void deleteById() {
        // Given
        final ID generatedId = (ID) TestUtils.generateString();
        doNothing().when(this.getRepository()).deleteById(generatedId);

        // When
        this.getService().delete(generatedId);

        // Then
        verify(this.getRepository()).deleteById(generatedId);
    }

    @Test
    public void deleteCollection() {
        // Given
        final Collection<ID> ids = Stream.of(this.generateEntity(), this.generateEntity()).map(E::getId).toList();
        doNothing().when(this.getRepository()).deleteAllById(ids);

        // When
        this.getService().delete(ids);

        // Then
        verify(this.getRepository()).deleteAllById(ids);
    }

    @Test
    public void existsById() {
        // Given
        final ID generatedId = (ID) TestUtils.generateString();
        when(this.getRepository().existsById(generatedId)).thenReturn(Boolean.TRUE);

        // When
        final boolean result = this.getService().exists(generatedId);

        // Then
        assertTrue(result);
    }

    @Test
    public void findAllEntities() {
        // Given
        final Collection<E> expectedEntities = Arrays.asList(this.generateEntity(), this.generateEntity());
        when(this.getRepository().findAll()).thenReturn(new ArrayList<>(expectedEntities));

        // When
        final Collection<E> resultEntities = this.getService().findAll();

        // Then
        assertIterableEquals(resultEntities, expectedEntities);
    }

    @Test
    public void findAllIds() {
        // Given
        final Collection<E> expectedEntities = Collections.singleton(this.generateEntity());
        final Collection<ID> generatedIds = Arrays.asList((ID) TestUtils.generateString(), (ID) TestUtils.generateString());
        when(this.getRepository().findAllById(generatedIds)).thenReturn(expectedEntities);

        // When
        final Collection<E> resultEntities = this.getService().findAll(generatedIds);

        // Then
        assertEquals(resultEntities.size(), expectedEntities.size());
        assertIterableEquals(resultEntities, expectedEntities);
    }

    @Test
    public void findOne() {
        // Given
        final Optional<E> optional = Optional.of(this.generateEntity());
        final ID generatedId = (ID) TestUtils.generateString();
        when(this.getRepository().findById(generatedId)).thenReturn(optional);

        // When
        final E result = this.getService().findOne(generatedId);

        // Then
        assertNotNull(result);
        assertEquals(result, optional.get());
    }

    @Test
    public void insert() {
        // Given
        final E entity = this.generateEntity();
        when(this.getRepository().insert(entity)).thenReturn(entity);

        // When
        final E result = this.getService().insert(entity);

        // Then
        assertNotNull(result);
        assertEquals(result, entity);
    }

    @Test
    public void insertEntities() {
        // Given
        final List<E> expectedEntities = Arrays.asList(this.generateEntity(), this.generateEntity());
        when(this.getRepository().insert(expectedEntities)).thenReturn(expectedEntities);

        // When
        final Collection<E> resultEntities = this.getService().insert(expectedEntities);

        // Then
        assertEquals(resultEntities.size(), expectedEntities.size());
        assertIterableEquals(resultEntities, expectedEntities);
    }

    @Test
    public void partialUpdate() {
        // Given
        final E entityOriginal = this.generateEntity();
        final E entityUpdated = this.generateEntity();
        final Map<String, Object> parameters = this.getParameters();

        this.mockPartialUpdate(entityOriginal, entityUpdated);

        // When
        final E result = this.getService().partialUpdate(parameters, entityOriginal.getId());

        // Then
        assertNotNull(result);
        assertEquals(result, entityUpdated);
    }

    protected void mockPartialUpdate(final E originalEntity, final E expectedEntity) {
        final Optional<E> optional = Optional.of(originalEntity);
        final E originalEntityCopy1 = GsonUtils.cloneObject(originalEntity, this.getClassEntity());
        final Optional<E> optionalCopy1 = Optional.of(originalEntityCopy1);
        when(this.getRepository().findById(originalEntity.getId())).thenReturn(optional).thenReturn(optionalCopy1);
        when(this.getRepository().save(originalEntityCopy1)).thenReturn(expectedEntity);
    }

    @Test
    public void save() {
        // Given
        final E originalEntity = this.generateEntity();
        final E expectedEntity = this.internalUpdate(GsonUtils.cloneObject(originalEntity, this.getClassEntity()), null);

        this.mockSave(originalEntity, expectedEntity);

        // When
        final E result = this.getService().save(expectedEntity);

        // Then
        assertNotNull(result);
        assertThat(result).isEqualTo(expectedEntity);
    }

    protected void mockSave(final E originalEntity, final E expectedEntity) {
        final Optional<E> optional = Optional.of(originalEntity);
        when(this.getRepository().findById(originalEntity.getId())).thenReturn(optional);
        when(this.getRepository().save(originalEntity)).thenReturn(expectedEntity);
    }

    @Test
    public void partialUpdateCollection() {
        // Given
        final Map<ID, Map<String, Object>> parametersIdentifiable = new HashMap<>();

        final E originalEntity1 = this.generateEntity();
        final Map<String, Object> parameters1 = this.getParameters();
        parametersIdentifiable.put(originalEntity1.getId(), parameters1);
        final E expectedEntity1 = this.internalUpdate(GsonUtils.cloneObject(originalEntity1, this.getClassEntity()), parameters1.values().stream().findFirst().get());

        final E originalEntity2 = this.generateEntity();
        final Map<String, Object> parameters2 = this.getParameters();
        parametersIdentifiable.put(originalEntity2.getId(), parameters2);
        final E expectedEntity2 = this.internalUpdate(GsonUtils.cloneObject(originalEntity2, this.getClassEntity()), parameters2.values().stream().findFirst().get());

        final Collection<E> expectedEntities = Arrays.asList(expectedEntity1, expectedEntity2);

        this.mockPartialUpdate(originalEntity1, expectedEntity1);
        this.mockPartialUpdate(originalEntity2, expectedEntity2);

        // When
        final Collection<E> resultEntities = this.getService().partialUpdate(parametersIdentifiable);

        // That
        assertEquals(resultEntities.size(), expectedEntities.size());
        assertThat(resultEntities).containsExactlyInAnyOrderElementsOf(expectedEntities);
    }

    @Test
    public void saveCollection() {
        // Given
        final E originalEntity1 = this.generateEntity();
        final E expectedEntity1 = this.internalUpdate(GsonUtils.cloneObject(originalEntity1, this.getClassEntity()), null);
        final E originalEntity2 = this.generateEntity();
        final E expectedEntity2 = this.internalUpdate(GsonUtils.cloneObject(originalEntity2, this.getClassEntity()), null);

        final Collection<E> expectedEntities = Arrays.asList(expectedEntity1, expectedEntity2);

        this.mockSave(originalEntity1, expectedEntity1);
        this.mockSave(originalEntity2, expectedEntity2);

        // When
        final Collection<E> resultEntities = this.getService().save(expectedEntities);

        // Then
        assertThat(resultEntities.size()).isEqualTo(expectedEntities.size());
        assertEquals(expectedEntities, resultEntities);
    }

    @Test
    public void countBy() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final DataContext dataContextExpected = new DataContext();
        dataContextExpected.setObject(this.generateEntity());
        final long resultExpected = TestUtils.generateLong();
        dataContextExpected.setResult(resultExpected);

        when(this.getRepository().count(dataContext)).thenReturn(dataContextExpected);

        // When
        final DataContext result = this.getService().count(dataContext);

        // Then
        assertEquals(result.getResult(), resultExpected);
        assertEquals(result.getObject(), dataContextExpected.getObject());
    }

    @Test
    public void existsBy() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final DataContext dataContextExpected = new DataContext();
        dataContextExpected.setObject(this.generateEntity());
        dataContextExpected.setResult(Boolean.TRUE);

        when(this.getRepository().exists(dataContext)).thenReturn(dataContextExpected);

        // When
        final DataContext result = this.getService().exists(dataContext);

        // Then
        assertNotNull(result);
        assertEquals(result.getResult(), Boolean.TRUE);
        assertEquals(result.getObject(), dataContextExpected.getObject());
    }

    @Test
    public void findAllBy() {
        // Given
        final DataContextPageable dataContext = new DataContextPageable();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final DataContextPageable dataContextExpected = new DataContextPageable();
        dataContextExpected.setObject(this.generateEntity());
        final List<E> dataContextResultExpected = new ArrayList<>();
        dataContextResultExpected.add(this.generateEntity());
        dataContextResultExpected.add(this.generateEntity());
        dataContextExpected.setResult(dataContextResultExpected);

        when(this.getRepository().findAll(dataContext)).thenReturn(dataContextExpected);

        // When
        final DataContextPageable dataContextResult = this.getService().findAll(dataContext);

        // Then
        assertNotNull(dataContextResult);
        final List<E> result = new ArrayList<>((Collection<E>) dataContextResult.getResult());
        assertEquals(result.size(), dataContextResultExpected.size());
        assertEquals(result.get(0), dataContextResultExpected.get(0));
        assertEquals(result.get(1), dataContextResultExpected.get(1));
        assertEquals(dataContextResult.getObject(), dataContextExpected.getObject());
    }

    @Test
    public void findOneBy() {
        // Given
        final DataContext dataContext = new DataContext();
        dataContext.setObject(this.generateEntity());
        dataContext.setCriteriaContent(new CriteriaContent());
        dataContext.getCriteriaContent().excludeAudit();

        final DataContext dataContextExpected = new DataContext();
        dataContextExpected.setObject(this.generateEntity());

        when(this.getRepository().findOne(dataContext)).thenReturn(dataContextExpected);

        // When
        final DataContext result = this.getService().findOne(dataContext);

        // Then
        assertNotNull(result);
        assertEquals(result.getResult(), dataContextExpected.getResult());
        assertEquals(result.getObject(), dataContextExpected.getObject());
    }

}

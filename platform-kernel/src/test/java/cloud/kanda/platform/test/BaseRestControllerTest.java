package cloud.kanda.platform.test;

import cloud.kanda.platform.dto.BaseDto;
import cloud.kanda.platform.entity.EntityDocument;
import cloud.kanda.platform.mappers.BaseMapStruct;
import cloud.kanda.platform.mappers.MapStructConverter;
import cloud.kanda.platform.rest.BaseRestController;
import cloud.kanda.platform.service.AbstractService;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The Class BaseController.
 *
 * @param <C>  the type controller
 * @param <S>  the type service
 * @param <D>  the type object dto
 * @param <E>  the type entity
 * @param <M>  the type mapper
 * @param <ID> the type identifier
 */
@SuppressWarnings("unchecked")
public abstract class BaseRestControllerTest<C extends BaseRestController<S, D, E, M, ID>,
        S extends AbstractService<E, ID>,
        D extends BaseDto,
        E extends EntityDocument<ID>,
        M extends BaseMapStruct<D, E>,
        ID extends Serializable>
        extends BaseControllerTest {

    public abstract Class<C> getClassController();

    public abstract Class<D> getClassDto();

    public abstract Class<E> getClassEntity();

    public abstract Class<M> getClassMapper();

    public abstract String getPath();

    public abstract S getService();

    private final ID RANDOM_ID = (ID) TestUtils.generateString();

    @SneakyThrows
    protected String getDataContextJson(final boolean withListObject) {
        final JSONParser parser = new JSONParser();
        final String jsonDataContext = this.getResourceContent("json/data-context.json");
        final Object objDataContext = parser.parse(jsonDataContext);
        final JSONObject jsonObjectDataContext = (JSONObject) objDataContext;
        jsonObjectDataContext.put("object", parser.parse(this.getObjectJson(withListObject)));
        return jsonObjectDataContext.toJSONString();
    }

    @SneakyThrows
    protected String getObjectJson(final boolean withListObject) {
        final JSONParser parser = new JSONParser();
        String jsonResult = null;
        final String jsonObject = this.getResourceContent("json".concat(this.getPath()).concat(".json"));
        final Object objTemp = parser.parse(jsonObject);
        if (objTemp instanceof JSONArray) {
            final JSONArray objectsJson = (JSONArray) objTemp;
            final Object obj = withListObject ? objectsJson : objectsJson.get(0);
            jsonResult = withListObject ? ((JSONArray) obj).toJSONString() : ((JSONObject) obj).toJSONString();
        }

        return jsonResult;
    }

    @SneakyThrows
    private String getResourceContent(final String resource) {
        return IOUtils.toString(this.getClassController().getClassLoader().getResourceAsStream(resource), StandardCharsets.UTF_8);
    }

    protected <T> T parse(final String json, final Class<T> clazz) {
        return TestUtils.jsonToObject(json, clazz);
    }

    protected <T> String parse(final T object) {
        return TestUtils.objectToJson(object);
    }

    @SneakyThrows
    protected <T> List<T> parseList(final String json, final Class<T> clazz) {
        final JSONParser parser = new JSONParser();
        final JSONArray objectsJson = (JSONArray) parser.parse(json);
        final List<T> listObject = new ArrayList<>();
        objectsJson.forEach(item -> {
            final JSONObject objectJson = (JSONObject) item;
            listObject.add(TestUtils.jsonToObject(objectJson.toJSONString(), clazz));
        });
        return listObject;
    }

    /* DataContext */

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void countBy() {
        // Given
        final String dataContextJson = this.getDataContextJson(Boolean.FALSE);
        final DataContext dataContextExpected = MapStructConverter.convertToEntity(this.parse(dataContextJson, DataContext.class), this.getClassMapper());
        dataContextExpected.setResult(TestUtils.generateLong());

        when(this.getService().count(ArgumentMatchers.any(DataContext.class))).thenReturn(dataContextExpected);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.post(this.getPath().concat("/count-by"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(dataContextJson))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().json(this.parse(dataContextExpected)));
        ;

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void existsBy() {
        // Given
        final String dataContextJson = this.getDataContextJson(Boolean.FALSE);
        final DataContext dataContextExpected = MapStructConverter.convertToEntity(this.parse(dataContextJson, DataContext.class), this.getClassMapper());
        dataContextExpected.setResult(Boolean.TRUE);

        when(this.getService().exists(ArgumentMatchers.any(DataContext.class))).thenReturn(dataContextExpected);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.post(this.getPath().concat("/exists-by"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(dataContextJson))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().json(this.parse(dataContextExpected)));

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void findAllBy() {
        // Given
        final String dataContextJson = this.getDataContextJson(Boolean.FALSE);
        final DataContextPageable dataContextExpected = (DataContextPageable) MapStructConverter.convertToEntity(this.parse(dataContextJson, DataContextPageable.class), this.getClassMapper());
        dataContextExpected.setResult(MapStructConverter.convertToEntities(this.parseList(this.getObjectJson(Boolean.TRUE), this.getClassDto()), this.getClassMapper()));

        when(this.getService().findAll(ArgumentMatchers.any(DataContextPageable.class))).thenReturn(dataContextExpected);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.post(this.getPath().concat("/find-all-by"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(dataContextJson))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().json(this.parse(dataContextExpected)));

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void findOneBy() {
        // Given
        final String dataContextJson = this.getDataContextJson(Boolean.FALSE);
        final DataContext dataContextExpected = MapStructConverter.convertToEntity(this.parse(dataContextJson, DataContext.class), this.getClassMapper());
        dataContextExpected.setResult(MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper()));

        when(this.getService().findOne(ArgumentMatchers.any(DataContext.class))).thenReturn(dataContextExpected);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.post(this.getPath().concat("/find-one-by"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(dataContextJson))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().json(this.parse(dataContextExpected)));

    }


    /* RESTFUL */

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void delete() {
        // Given
        final E entity = MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper());

        when(this.getService().findOne(this.RANDOM_ID)).thenReturn(entity);
        doNothing().when(this.getService()).delete(this.RANDOM_ID);
        when(this.getService().findOne(this.RANDOM_ID)).thenReturn(null);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.delete(this.getPath().concat("/").concat(this.RANDOM_ID.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isNoContent());

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void find() {
        // Given
        final E entity = MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper());

        when(this.getService().findOne(this.RANDOM_ID)).thenReturn(entity);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.getPath().concat("/").concat(this.RANDOM_ID.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk());

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void findAll() {
        // Given
        final List<E> entities = (List<E>) MapStructConverter.convertToEntities(this.parseList(this.getObjectJson(Boolean.TRUE), this.getClassDto()), this.getClassMapper());

        when(this.getService().findAll()).thenReturn(entities);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.getPath())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk());

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void insert() {
        // Given
        final String jsonObject = this.getObjectJson(Boolean.FALSE);
        final E entity = MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper());

        when(this.getService().insert(ArgumentMatchers.any(this.getClassEntity()))).thenReturn(entity);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.post(this.getPath())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(jsonObject))
                // Then
                .andExpect(status().isCreated());

    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void save() {
        // Given
        final String jsonObject = this.getObjectJson(Boolean.FALSE);
        final E entity = MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper());

        when(this.getService().save(ArgumentMatchers.any(this.getClassEntity()))).thenReturn(entity);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.put(this.getPath())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(jsonObject))
                // Then
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void updatePartial() {
        // Given
        final E entity = MapStructConverter.convertToEntity(this.parse(this.getObjectJson(Boolean.FALSE), this.getClassDto()), this.getClassMapper());
        final Map<String, Object> map = new HashMap<>();
        final String jsonMap = new JSONObject(map).toJSONString();

        when(this.getService().partialUpdate(map, this.RANDOM_ID)).thenReturn(entity);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.patch(this.getPath().concat("/").concat(this.RANDOM_ID.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(jsonMap))
                // Then
                .andExpect(status().isOk());

    }


    /* OTHERS */

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void count() {
        // Given
        when(this.getService().count()).thenReturn(TestUtils.generateLong());

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.getPath().concat("/count"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(username = "test", authorities = {"READ"})
    public void existsId() {
        // Given
        when(this.getService().exists(this.RANDOM_ID)).thenReturn(Boolean.TRUE);

        // When
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.getPath().concat("/exists-id/").concat(this.RANDOM_ID.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk());
    }

}

package cloud.kanda.platform.test.security.util;

import lombok.experimental.UtilityClass;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UtilityClass
public class SecurityTestUtils {

    public void mockApplicationUser() {
        final KeycloakPrincipal<RefreshableKeycloakSecurityContext> principal = mock(KeycloakPrincipal.class);
        final Authentication authentication = mock(KeycloakAuthenticationToken.class);
        final SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);
        final RefreshableKeycloakSecurityContext refreshableKeycloakSecurityContext = mock(RefreshableKeycloakSecurityContext.class);
        when(principal.getKeycloakSecurityContext()).thenReturn(refreshableKeycloakSecurityContext);
        final AccessToken accessToken = mock(AccessToken.class);
        when(principal.getKeycloakSecurityContext().getToken()).thenReturn(accessToken);
    }

    public void mockApplicationUserRoles(final Set<String> roles) {
        final KeycloakPrincipal<RefreshableKeycloakSecurityContext> principal = mock(KeycloakPrincipal.class);
        final Authentication authentication = mock(KeycloakAuthenticationToken.class);
        final SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);
        final RefreshableKeycloakSecurityContext refreshableKeycloakSecurityContext = mock(RefreshableKeycloakSecurityContext.class);
        when(principal.getKeycloakSecurityContext()).thenReturn(refreshableKeycloakSecurityContext);
        final AccessToken accessToken = mock(AccessToken.class);
        when(principal.getKeycloakSecurityContext().getToken()).thenReturn(accessToken);
        final AccessToken.Access access = mock(AccessToken.Access.class);
        when(accessToken.getRealmAccess()).thenReturn(access);
        when(accessToken.getRealmAccess().getRoles()).thenReturn(roles);
    }
}

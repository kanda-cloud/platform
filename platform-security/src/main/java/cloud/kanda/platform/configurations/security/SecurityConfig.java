package cloud.kanda.platform.configurations.security;

import cloud.kanda.platform.security.bos.PathRolesMapperBO;
import lombok.RequiredArgsConstructor;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@RequiredArgsConstructor
@KeycloakConfiguration
@EnableConfigurationProperties(KeycloakSpringBootProperties.class)
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {
	
    public final KeycloakClientRequestFactory keycloakClientRequestFactory;
	
	public final IEndpointsConfig endpointsConfig;

	@Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public KeycloakRestTemplate keycloakRestTemplate() {
        return new KeycloakRestTemplate(this.keycloakClientRequestFactory);
    }

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		KeycloakAuthenticationProvider keycloakAuthenticationProvider = new CustomKeycloakAuthenticationProvider();
		// adding proper authority mapper for prefixing role with "ROLE_"
		keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
		auth.authenticationProvider(keycloakAuthenticationProvider);
	}

	@Bean
	@Override
	protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
		return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		super.configure(http);
		http.csrf().disable().oauth2Client();
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry = http
			.authorizeRequests()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			.antMatchers(HttpMethod.POST, this.endpointsConfig.getPostWhiteList()).permitAll()
			.antMatchers(this.endpointsConfig.getSwaggerWhiteList()).permitAll()
			.antMatchers(this.endpointsConfig.getGetWhiteList()).permitAll();
			
		urlRegistry = this.addPathAnyRoleRestriction(urlRegistry);
		urlRegistry = this.addPathAnyAuthoritiesRestriction(urlRegistry);
		
		urlRegistry.anyRequest().authenticated();
	}
	
	private ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry addPathAnyRoleRestriction(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry) throws Exception {
		for (final PathRolesMapperBO pathRoles: this.endpointsConfig.getPathAnyRoles()) {
			urlRegistry = urlRegistry.antMatchers(pathRoles.getPath()).hasAnyRole(pathRoles.getRoles());
		}
		
		return urlRegistry;
	}
	
	private ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry addPathAnyAuthoritiesRestriction(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry) throws Exception {
		for (final PathRolesMapperBO pathRoles: this.endpointsConfig.getPathAnyAuthorities()) {
			urlRegistry = urlRegistry.antMatchers(pathRoles.getPath()).hasAnyAuthority(pathRoles.getAuthorities());
		}
		
		return urlRegistry;
	}
	
}

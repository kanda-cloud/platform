package cloud.kanda.platform.configurations.security;

import cloud.kanda.platform.security.bos.PathRolesMapperBO;
import cloud.kanda.platform.security.enums.RolesEnum;

public interface IEndpointsConfig {
	
	String[] GET_WHITELIST_DEFAULT = {
			"/api/build-version",
			"/api/is-maintenance-mode",
			"/api/ping",
			"/socket/**",
			"/loggers/**",
			"/actuator/**"
    };
	
	String[] POST_WHITELIST_DEFAULT = {
	};
	
	PathRolesMapperBO[] PATH_ANY_ROLES_DEFAULT = {
			PathRolesMapperBO.builder().path("/api/toggle-maintenance-mode")
								   	   .roles(new String[] {RolesEnum.ROLE_ADMIN.getRole(),
														    RolesEnum.ROLE_RAGNAROK.getRole()}).build()
    };

	PathRolesMapperBO[] PATH_ANY_AUTHORITIES_DEFAULT = {};
	
	String[] SWAGGER_WHITELIST = {
			"/swagger-resources/**",
            "/documentation/**",
            "/swagger-ui.html",
            "/swagger-ui/**",
            "/v3/api-docs/**",
            "/webjars/**"
    };

	default String[] getGetWhiteList() {
		return GET_WHITELIST_DEFAULT;
	}
	
	default String[] getPostWhiteList() {
		return POST_WHITELIST_DEFAULT;
	}
	
	default PathRolesMapperBO[] getPathAnyRoles() {
		return PATH_ANY_ROLES_DEFAULT;
	}
	
	default PathRolesMapperBO[] getPathAnyAuthorities() {
		return PATH_ANY_AUTHORITIES_DEFAULT;
	}
	
	default String[] getSwaggerWhiteList() {
		return SWAGGER_WHITELIST;
	}

}

package cloud.kanda.platform.configurations.security;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@ConditionalOnProperty(value = "scan.packages.mongo.repository")
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    @SuppressWarnings("unchecked")
    public Optional<String> getCurrentAuditor() {
        final KeycloakPrincipal<RefreshableKeycloakSecurityContext> authentication = (KeycloakPrincipal<RefreshableKeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final String userId = authentication != null ? authentication.getKeycloakSecurityContext().getToken().getSubject() : null;

        return Optional.ofNullable(userId);
    }
}

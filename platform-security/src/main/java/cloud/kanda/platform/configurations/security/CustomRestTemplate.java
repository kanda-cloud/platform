package cloud.kanda.platform.configurations.security;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;

@ConditionalOnProperty(name = "ndk.auth.controller", matchIfMissing = true)
@Configuration
public class CustomRestTemplate {

	private final String keyStorePassword;
	private final String keyStorePath;
	private final GsonHttpMessageConverter gsonHttpMessageConverter;

	public CustomRestTemplate(@Value("${server.ssl.key-store-password}")
							  final String keyStorePassword,
							  @Value("${server.ssl.key-store}")
							  final String keyStorePath,
							  final GsonHttpMessageConverter gsonHttpMessageConverter) {
		super();
		this.keyStorePassword = keyStorePassword;
		this.keyStorePath = keyStorePath;
		this.gsonHttpMessageConverter = gsonHttpMessageConverter;
	}
		
	@Bean
	public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, UnrecoverableKeyException, CertificateException, IOException {
		final TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        final SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
        sslContextBuilder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

        final SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
																   	 .loadKeyMaterial(ResourceUtils.getFile(keyStorePath),
																					  keyStorePassword.toCharArray(),
																	 				  keyStorePassword.toCharArray())
																	 .loadTrustMaterial(null, acceptingTrustStrategy)
																   	 .build();

        final SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        final CloseableHttpClient httpClient = HttpClients.custom()
									        		.setSSLSocketFactory(csf)
									        		.setSSLContext(sslContextBuilder.build())
									        		.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
									        		.build();

        final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        final RestTemplate restTemplate = new RestTemplate(requestFactory);
	    restTemplate.setMessageConverters(Collections.singletonList(gsonHttpMessageConverter));

	    return restTemplate;
	 }
	
}

package cloud.kanda.platform.configurations.security;

import org.keycloak.adapters.springsecurity.account.KeycloakRole;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;

import java.util.ArrayList;
import java.util.Collection;

public class CustomKeycloakAuthenticationProvider extends KeycloakAuthenticationProvider {

    private GrantedAuthoritiesMapper grantedAuthoritiesMapper;

	@Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) authentication;
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

        for (String role : token.getAccount().getRoles()) {
            grantedAuthorities.add(new KeycloakRole(role));
        }
        
        // Adding roles from realm

        for (String role : token.getAccount().getKeycloakSecurityContext().getToken().getRealmAccess().getRoles()) {
        	// Only accept roles from realm with prefix ROLE_
            final String prefix = "ROLE_";
            if (role.startsWith(prefix)) {
        		grantedAuthorities.add(new KeycloakRole(role));
        	}
        }
        
        return new KeycloakAuthenticationToken(token.getAccount(), token.isInteractive(), this.mapAuthorities(grantedAuthorities));
    }
	
	private Collection<? extends GrantedAuthority> mapAuthorities(
            Collection<? extends GrantedAuthority> authorities) {
        return this.grantedAuthoritiesMapper != null
            ? this.grantedAuthoritiesMapper.mapAuthorities(authorities)
            : authorities;
    }
	
}

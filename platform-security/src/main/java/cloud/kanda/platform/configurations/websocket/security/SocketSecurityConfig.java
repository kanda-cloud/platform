package cloud.kanda.platform.configurations.websocket.security;

import cloud.kanda.platform.configurations.websocket.IEndpointsWSConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

import java.util.Arrays;

@Configuration
public class SocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Value("${cors.origins}")
    private String corsOrigins;

    @Autowired
    private IEndpointsWSConfig endpointsWSConfig;

    @Override
    protected void configureInbound(final MessageSecurityMetadataSourceRegistry messages) {
        messages.simpSubscribeDestMatchers(this.endpointsWSConfig.getSimpSubscribeDestMatchers()).permitAll();

        this.buildSimpDestMatchersAuthenticated(messages);
        this.buildSimpDestMatchersPermitAll(messages);
    }

    @Override
    protected boolean sameOriginDisabled() {
        return this.endpointsWSConfig.sameOriginDisabled();
    }

    private void buildSimpDestMatchersAuthenticated(final MessageSecurityMetadataSourceRegistry messages) {
        final String[] matchers = this.buildSimpDestMatchers(this.endpointsWSConfig.getSimpDestMatchersAuthenticated());
        if (matchers.length > 0) {
            messages.simpDestMatchers(matchers).authenticated();
        }
    }

    private void buildSimpDestMatchersPermitAll(final MessageSecurityMetadataSourceRegistry messages) {
        final String[] matchers = this.buildSimpDestMatchers(this.endpointsWSConfig.getSimpDestMatchersPermitAll());
        if (matchers.length > 0) {
            messages.simpDestMatchers(matchers).permitAll();
        }
    }

    private String[] buildSimpDestMatchers(final String[] simpDest) {
        return Arrays.stream(simpDest).map(sd -> this.contextPath.concat(sd)).toArray(String[]::new);
    }

}

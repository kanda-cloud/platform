package cloud.kanda.platform.security.utils;

import lombok.experimental.UtilityClass;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collection;

@UtilityClass
public class SecurityUtils {

    public KeycloakAuthenticationToken getAuthentication() {
        return (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    }

    @SuppressWarnings("unchecked")
    public KeycloakPrincipal<RefreshableKeycloakSecurityContext> getPrincipal() {
        return (KeycloakPrincipal<RefreshableKeycloakSecurityContext>) getAuthentication().getPrincipal();
    }

    public AccessToken getUser() {
        final KeycloakPrincipal<RefreshableKeycloakSecurityContext> principal = getPrincipal();
        return principal.getKeycloakSecurityContext().getToken();
    }

    public String getUserId() {
        return getPrincipal().getName();
    }

    public Collection<String> getAllRoles() {
        return getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
    }

    public boolean hasRole(@NotNull final String role) {
        return getAllRoles().stream().anyMatch(roleUser -> roleUser.equals(role));
    }

    public Collection<String> getScopes() {
        return Arrays.asList(getUser().getScope().split(" "));
    }

    public String getClientId() {
        return getPrincipal().getKeycloakSecurityContext().getDeployment().getResourceName();
    }

    //	public Collection<Role> getUserRolesClientId(@NotNull String clientId) {
//		Collection<Role> roles = getUserRoles();
//		if (roles.isEmpty()) {
//			return Lists.newArrayList(getRoleService().findRoleSystemByName(RolesEnum.ROLE_AUTH_ANONYMOUS.toString()));
//		} else {
//			return filterUserRolesClientId(clientId, roles);
//		}
//	}
//	
//	private Collection<Role> filterUserRolesClientId(@NotNull String clientId, Collection<Role> roles) {
//		ClientDetail clientDetail = getClientDetail(clientId);
//		Collection<String> ids = getRoleService().findRoleSystem().stream().map(Role::getId).collect(Collectors.toList());
//		Collection<Role> rolesApp = clientDetail.getRoles();
//		ids.addAll(rolesApp.stream().map(Role::getId).collect(Collectors.toList()));
//		Set<Role> rolesUserApp = new HashSet<>();
//		for (Role role: roles) {
//			if (ids.contains(role.getId())) {
//				rolesUserApp.add(role);
//			}
//		}
//		
//		return rolesUserApp;
//	}
//	
//	public Boolean isAdmin() {
//		if (isUserLogged()) {
//			return getUserRoles().stream().anyMatch(item -> item.getName().equals(RolesEnum.ROLE_AUTH_ADMIN.toString()) && item.getUserOwner() == null);
//		}
//		return Boolean.FALSE;
//	}
//	
//	public Boolean isDev() {
//		if (isUserLogged()) {
//			return getUserRoles().stream().anyMatch(item -> item.getName().equals(RolesEnum.ROLE_AUTH_DEV.toString()) && item.getUserOwner() == null);
//		}
//		return Boolean.FALSE;
//	}
//	
    public String getUsername() {
        return getPrincipal().getKeycloakSecurityContext().getToken().getPreferredUsername();
    }
//	
//	public OAuth2Authentication buildOAuth2Authentication(UserDetails user) {
//		OAuth2Authentication authentication = getOauthAuthentication();
//		return buildOAuth2Authentication(new OAuth2AccessTokenDTO(authentication, user));
//	}

//	public OAuth2Authentication buildOAuth2Authentication(OAuth2AccessTokenDTO oAuth2AccessTokenDTO) {
//		OAuth2Request oAuth2Request = new OAuth2Request(oAuth2AccessTokenDTO.getRequestParameters(),
//														oAuth2AccessTokenDTO.getClientId(),
//														oAuth2AccessTokenDTO.getAuthorities(),
//														oAuth2AccessTokenDTO.isApproved(),
//														oAuth2AccessTokenDTO.getScope(),
//														oAuth2AccessTokenDTO.getResourceIds(),
//														oAuth2AccessTokenDTO.getRedirectUri(),
//														oAuth2AccessTokenDTO.getResponseTypes(),
//														oAuth2AccessTokenDTO.getExtension());
//
//		Authentication userAuthentication = new UsernamePasswordAuthenticationToken(oAuth2AccessTokenDTO.getUser(),
//																					oAuth2AccessTokenDTO.getCredentials(),
//																					oAuth2AccessTokenDTO.getAuthorities());
//
//		return new OAuth2Authentication(oAuth2Request, userAuthentication);
//	}

//	public void updateAuthentication(UserDetails user) {
//		OAuth2Authentication authentication = getOauthAuthentication();
//		Authentication userAuthentication = new UsernamePasswordAuthenticationToken(user,
//																					authentication.getCredentials(),
//																					authentication.getUserAuthentication().getAuthorities());
//	
//		SecurityContextHolder.getContext().setAuthentication(new OAuth2Authentication(authentication.getOAuth2Request(), userAuthentication));
//	}

    public void setAuthentication(final Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

//	public ClientDetail getClientDetail(@NotNull String clientId) {
//		return getClientDetailService().findByClientId(clientId);
//	}


}

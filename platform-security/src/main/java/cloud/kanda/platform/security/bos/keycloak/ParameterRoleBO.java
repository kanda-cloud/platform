package cloud.kanda.platform.security.bos.keycloak;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class ParameterRoleBO {

    private Boolean briefRepresentation;
    private Integer first;
    private Integer max;
    private String search;

}

package cloud.kanda.platform.security.bos.keycloak;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class ParameterUserBaseBO {

    private String email;
    private String firstName;
    private String lastName;
    private String search;
    private String username;

}

package cloud.kanda.platform.security.bos.keycloak;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ParameterExecuteActionsEmailBO {

    private String client_id;
    private Integer lifespan;
    private String redirect_uri;

}

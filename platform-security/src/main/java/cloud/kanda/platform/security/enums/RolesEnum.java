package cloud.kanda.platform.security.enums;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RolesEnum {
	
	ROLE_ADMIN("ADMIN"),
	ROLE_ANONYMOUS("ANONYMOUS"), 
	ROLE_DEV("DEV"),
	ROLE_RAGNAROK("RAGNAROK"),
	ROLE_USER("USER");
	
	private String role;
	
	public static RolesEnum find(final String role) {
	    return Arrays.stream(values()).filter(value -> value.toString().equals(role)).findFirst().orElse(null);
	}
	
	public static Boolean exists(final String role) {
	    return find(role) != null;
	}
	
}

package cloud.kanda.platform.security.bos.keycloak;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class ParameterClientBO {

    private String id;
    private String clientId;
    private Integer first;
    private Integer max;
    private Boolean search;
    private Boolean viewableOnly;
    private String attr;

}

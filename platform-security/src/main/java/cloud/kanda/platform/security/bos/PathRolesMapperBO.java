package cloud.kanda.platform.security.bos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PathRolesMapperBO {

	private String path;
	
	private String[] roles;
	
	private String[] authorities;
		
}

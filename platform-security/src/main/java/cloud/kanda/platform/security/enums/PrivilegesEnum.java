package cloud.kanda.platform.security.enums;

import java.util.Arrays;

public enum PrivilegesEnum {
	
	PRIVILEGE_DELETE,
	PRIVILEGE_DEVELOPER, 
	PRIVILEGE_FULL,
	PRIVILEGE_READ,
	PRIVILEGE_TRUST,
	PRIVILEGE_WRITE;
	
	public static PrivilegesEnum find(final String privilege){
	    return Arrays.stream(values()).filter(value -> value.toString().equals(privilege)).findFirst().orElse(null);
	}
	
	public static Boolean exists(final String privilege){
	    return find(privilege) != null;
	}
	
}

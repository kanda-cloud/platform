package cloud.kanda.platform.restClient.utils;

import cloud.kanda.platform.utils.reflex.UtilsReflex;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@UtilityClass
@Slf4j
public class RestClientUtils {

    public <O> String convertObjectToRequestParam(final O object) {
        final Collection<Field> allFields = UtilsReflex.getFieldsClass(object.getClass(), Boolean.TRUE);
        String requestParam = "?";
        boolean isFirst = Boolean.TRUE;
        for (final Field field : allFields) {
            ReflectionUtils.makeAccessible(field);
            Object value = null;
            try {
                value = field.get(object);
            } catch (final IllegalArgumentException | IllegalAccessException e) {
                log.warn("Error al leer el valor de la propiedad {0}", field.getName());
            }
            if (value != null) {
                if (!isFirst) {
                    requestParam = requestParam.concat("&");
                }
                requestParam = requestParam.concat(field.getName()).concat("=").concat(value.toString());
                isFirst = Boolean.FALSE;
            }
        }

        return requestParam;
    }

    public String buildRequestParams(final Map<String, String> params) {
        final AtomicReference<String> path = new AtomicReference<>("?");
        params.forEach((name, value) -> path.set(String.format("%s%s=%s&", path.get(), name, value)));

        return StringUtils.chop(path.get());
    }

    public <ID extends Serializable> URI createURI(final String serverUrl,
                                                   final String mainPath,
                                                   final String endpointPath,
                                                   final ID id) {
        return URI.create(buildPath(serverUrl, mainPath, endpointPath, id));
    }

    public URI createURI(final String serverUrl,
                         final String mainPath,
                         final String endpointPath) {
        return URI.create(buildPath(serverUrl, mainPath, endpointPath));
    }

    public String buildPath(final String serverUrl,
                            final String mainPath,
                            final String endpointPath) {
        return String.format("%s%s%s", serverUrl, mainPath, endpointPath);
    }

    public <ID extends Serializable> String buildPath(final String serverUrl,
                                                      final String mainPath,
                                                      final String endpointPath,
                                                      final ID id) {
        return String.format("%s%s%s%s", serverUrl, mainPath, endpointPath, id);
    }

}

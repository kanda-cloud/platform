package cloud.kanda.platform.restClient.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class HeaderConstants {

    public final String CLIENT_ID = "Client-id";
}

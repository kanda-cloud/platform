package cloud.kanda.platform.restClient.restTemplate;

import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;

@ConditionalOnProperty(name = "ms-keycloak.user-name", matchIfMissing = true)
@Configuration
public class KeycloakAuthRestTemplate extends RestTemplate {

    private final String serverUrl;
    private final String realm;
    private final String userName;
    private final String userPassword;
    private final String clientId;
    private final String clientSecret;
    private final GsonHttpMessageConverter gsonHttpMessageConverter;

    public KeycloakAuthRestTemplate(@Value("${keycloak.auth-server-url}")
                                    final String serverUrl,
                                    @Value("${keycloak.realm}")
                                    final String realm,
                                    @Value("${ms-keycloak.user-name}")
                                    final String userName,
                                    @Value("${ms-keycloak.user-password}")
                                    final String userPassword,
                                    @Value("${keycloak.resource}")
                                    final String clientId,
                                    @Value("${ms-keycloak.secret}")
                                    final String clientSecret,
                                    final GsonHttpMessageConverter gsonHttpMessageConverter) {
        super();
        this.serverUrl = serverUrl;
        this.realm = realm;
        this.userName = userName;
        this.userPassword = userPassword;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.gsonHttpMessageConverter = gsonHttpMessageConverter;
        this.setMessageConverters(Collections.singletonList(gsonHttpMessageConverter));
    }
    @Override
    public <T> ResponseEntity<T> getForEntity(final URI uri, final Class<T> responseType) {
        return this.authorizedRestCall(uri, null, responseType, HttpMethod.GET);
    }

    @Override
    public <T> ResponseEntity<T> postForEntity(final URI uri, final Object body, final Class<T> responseType) {
        return this.authorizedRestCall(uri, body, responseType, HttpMethod.POST);
    }

    public <T> ResponseEntity<T> putForEntity(final URI uri, final Object body, final Class<T> responseType) {
        return this.authorizedRestCall(uri, body, responseType, HttpMethod.PUT);
    }

    private <T> ResponseEntity<T> authorizedRestCall(final URI uri, final Object body, final Class<T> responseType, final HttpMethod httpMethod) {
        final Keycloak keycloak = this.getKeycloakInstance();
        final HttpEntity<?> request = this.getRequest(keycloak, body);
        final ResponseEntity<T> response = this.exchange(uri, httpMethod, request, responseType);
        keycloak.close();

        return response;
    }

    public <T> HttpEntity<?> getRequest(final Keycloak keycloak, final T body) {
        return new HttpEntity<>(body, this.getHeaders(keycloak));
    }

    private HttpHeaders getHeaders(final Keycloak keycloak) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.AUTHORIZATION, "bearer " + this.getAccessToken(keycloak));

        return headers;
    }

    private String getAccessToken(final Keycloak keycloak) {
        return keycloak.tokenManager().getAccessTokenString();
    }

    public Keycloak getKeycloakInstance() {
        return Keycloak.getInstance(this.serverUrl, this.realm, this.userName, this.userPassword, this.clientId, this.clientSecret);
    }

}

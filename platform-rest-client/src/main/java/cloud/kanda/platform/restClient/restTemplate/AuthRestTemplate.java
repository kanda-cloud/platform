package cloud.kanda.platform.restClient.restTemplate;

import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@RequiredArgsConstructor
public class AuthRestTemplate extends RestTemplate {

    private final KeycloakAuthRestTemplate keycloakAuthRestTemplate;

    @Override
    public <T> ResponseEntity<T> getForEntity(final URI uri, final Class<T> responseType) {
        return this.authorizedRestCall(uri, null, responseType, HttpMethod.GET);
    }

    @Override
    public <T> ResponseEntity<T> postForEntity(final URI uri, final Object body, final Class<T> responseType) {
        return this.authorizedRestCall(uri, body, responseType, HttpMethod.POST);
    }

    public <T> ResponseEntity<T> putForEntity(final URI uri, final Object body, final Class<T> responseType) {
        return this.authorizedRestCall(uri, body, responseType, HttpMethod.PUT);
    }

    private <T> ResponseEntity<T> authorizedRestCall(final URI uri, final Object body, final Class<T> responseType, final HttpMethod httpMethod) {
        final Keycloak keycloak = this.keycloakAuthRestTemplate.getKeycloakInstance();
        final HttpEntity<?> request = this.getRequest(keycloak, body);
        final ResponseEntity<T> response = this.exchange(uri, httpMethod, request, responseType);
        keycloak.close();

        return response;
    }

    private <T> HttpEntity<?> getRequest(final Keycloak keycloak, final T body) {
        return this.keycloakAuthRestTemplate.getRequest(keycloak, body);
    }

}

package cloud.kanda.platform.restClient.restTemplate;

import cloud.kanda.platform.dto.BaseDto;
import cloud.kanda.platform.rest.ApiEndpointsConstants;
import cloud.kanda.platform.restClient.utils.RestClientUtils;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.net.URI;
import java.util.Collection;
import java.util.Map;

@AllArgsConstructor
public abstract class AbstractRestClientTemplate<O extends BaseDto, ID extends Serializable> {

    protected final String serverUrl;
    protected final String mainPath;
    protected final RestTemplate restTemplate;

    /* DataContext */

    public DataContext countBy(final DataContext dataContext) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.COUNT_BY);
        return this.restTemplate.postForEntity(uri, dataContext, DataContext.class).getBody();
    }

    public DataContext existsBy(final DataContext dataContext) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.EXISTS_BY);
        return this.restTemplate.postForEntity(uri, dataContext, DataContext.class).getBody();
    }

    public DataContextPageable findAllBy(final DataContextPageable dataContext) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.FIND_ALL_BY);
        return this.restTemplate.postForEntity(uri, dataContext, DataContextPageable.class).getBody();
    }

    public DataContext findOneBy(final DataContext dataContext) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.FIND_ONE_BY);
        return this.restTemplate.postForEntity(uri, dataContext, DataContext.class).getBody();
    }

    /* RESTFUL */

    public void delete(final ID id) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.SLASH, id);
        this.restTemplate.delete(uri);
    }

    public O findOne(final ID id) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.SLASH, id);
        return this.restTemplate.getForEntity(uri, this.getObjectClass()).getBody();
    }

    public Collection<?> findAll() {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, Strings.EMPTY);
        return this.restTemplate.getForEntity(uri, Collection.class).getBody();
    }

    public O insert(final O object) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, Strings.EMPTY);
        return this.restTemplate.postForEntity(uri, object, this.getObjectClass()).getBody();
    }

    public void save(final O object) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, Strings.EMPTY);
        this.restTemplate.put(uri, object);
    }

    public void bulkSave(final Collection<O> objects) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.BULK_SAVE);
        this.restTemplate.put(uri, objects);
    }

    public O partialUpdate(final ID id,
                           final Map<String, Object> parameters) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.SLASH, id);
        return this.restTemplate.patchForObject(uri, parameters, this.getObjectClass());
    }

    public Long count() {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.COUNT);
        return this.restTemplate.getForEntity(uri, Long.class).getBody();
    }

    public Boolean existsId(final ID id) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, String.format("%s%s", ApiEndpointsConstants.EXISTS_ID, ApiEndpointsConstants.SLASH), id);
        return this.restTemplate.getForEntity(uri, Boolean.class).getBody();
    }

    public Collection<?> partialUpdate(final Map<ID, Map<String, Object>> parameters) {
        final URI uri = RestClientUtils.createURI(this.serverUrl, this.mainPath, ApiEndpointsConstants.ALL);
        return this.restTemplate.patchForObject(uri, parameters, Collection.class);
    }

    public abstract Class<O> getObjectClass();

}
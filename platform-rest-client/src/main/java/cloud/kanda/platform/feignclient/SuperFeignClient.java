package cloud.kanda.platform.feignclient;

import cloud.kanda.platform.dto.BaseDto;
import cloud.kanda.platform.rest.ApiEndpointsConstants;
import cloud.kanda.platform.utils.data.DataContext;
import cloud.kanda.platform.utils.data.DataContextPageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public interface SuperFeignClient<D extends BaseDto, I extends Serializable> {

    /* DataContext */

    @PostMapping(ApiEndpointsConstants.COUNT_BY)
    DataContext countBy(@RequestBody DataContext dataContext);

    @PostMapping(ApiEndpointsConstants.EXISTS_BY)
    DataContext existsBy(@RequestBody DataContext dataContext);

    @PostMapping(ApiEndpointsConstants.FIND_ALL_BY)
    DataContextPageable findAllBy(@RequestBody DataContextPageable dataContext);

    @PostMapping(ApiEndpointsConstants.FIND_ONE_BY)
    DataContext findOneBy(@RequestBody DataContext dataContext);

    /* RESTFUL */

    @DeleteMapping("/{id}")
    D delete(@PathVariable I id);

    @GetMapping("/{id}")
    D find(@PathVariable I id);

    @GetMapping
    Collection<D> find();

    @PostMapping
    D insert(@NotNull @RequestBody D object);

    @PutMapping
    D save(@RequestBody D object);

    @PutMapping(ApiEndpointsConstants.BULK_SAVE)
    Collection<D> bulkSave(@RequestBody Collection<D> objects);

    @PatchMapping("/{id}")
    D partialUpdate(@RequestBody Map<String, Object> parameters, @PathVariable I id);

    /* OTHERS */

    @GetMapping(ApiEndpointsConstants.COUNT)
    Long count();

    @GetMapping(ApiEndpointsConstants.EXISTS_ID + "/{id}")
    Boolean existsId(@PathVariable I id);

    @PatchMapping(ApiEndpointsConstants.ALL)
    Collection<D> partialUpdate(@RequestBody Map<I, Map<String, Object>> parameters);

}

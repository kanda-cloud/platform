package cloud.kanda.platform.feignclient.configurations;

import feign.RequestInterceptor;
import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
public class CustomFeignConfiguration {

    @Value("${server.ssl.key-store-password}")
    private String keyStorePassword;

    @Value("${server.ssl.key-store}")
    private String keyStorePath;

    @Value("${server.ssl.key-store-type}")
    private String keyStoreType;

    @Value("${keycloak.auth-server-url}")
    private String serverUrl;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${ms-keycloak.user-name}")
    private String userName;

    @Value("${ms-keycloak.user-password}")
    private String userPassword;

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${spring.security.oauth2.client.registration.${keycloak.resource}.client-secret}")
    private String clientSecret;

    @Bean
    public void sslFeignConfig() {
        System.setProperty("javax.net.ssl.keyStoreType", keyStoreType);
        System.setProperty("javax.net.ssl.keyStore", keyStorePath);
        System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", getAccessToken()));
//            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//
//            if (authentication != null && authentication.getDetails() instanceof SimpleKeycloakAccount details) {
//                requestTemplate.header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", details.getPrincipal()));
//            }
        };
    }

    private String getAccessToken() {
        return getKeycloakInstance().tokenManager().getAccessTokenString();
    }

    private Keycloak getKeycloakInstance() {
        return Keycloak.getInstance(this.serverUrl, this.realm, this.userName, this.userPassword, this.clientId, this.clientSecret);
    }

}

package cloud.kanda.platform.utils.network;

import lombok.experimental.UtilityClass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

@UtilityClass
public class NetworkUtils {

    public String getNATIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (final UnknownHostException e) {
            return null;
        }
    }

    public String getPublicIP() {
        try {
            final URL publicIp = new URL(Constants.URI_PUBLIC_IP);
            final BufferedReader in = new BufferedReader(new InputStreamReader(publicIp.openStream()));
            return in.readLine();

        } catch (final Exception e) {
            return null;
        }
    }

}

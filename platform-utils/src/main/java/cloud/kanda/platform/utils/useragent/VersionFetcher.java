/*
 * 
 */
package cloud.kanda.platform.utils.useragent;

/**
 * Interaface that gets string and returns extrancted version .
 *
 * @author alexr
 */
interface VersionFetcher {
	
	/**
	 * Version.
	 *
	 * @param str the str
	 * @return the version
	 */
	Version version(String str);
}

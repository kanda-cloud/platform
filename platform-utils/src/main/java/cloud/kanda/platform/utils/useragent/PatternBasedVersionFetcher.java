/*
 * 
 */
package cloud.kanda.platform.utils.useragent;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extracts version from provided UserAgent string using pattern.  
 * @author alexr
 */
class PatternBasedVersionFetcher implements VersionFetcher {
	
	/** The pattern. */
	private final Pattern pattern;

	/**
	 * Instantiates a new pattern based version fetcher.
	 *
	 * @param regex the regex
	 */
	PatternBasedVersionFetcher(String regex) {
		this(Pattern.compile(regex, CASE_INSENSITIVE));
	}
	
	/**
	 * Instantiates a new pattern based version fetcher.
	 *
	 * @param pattern the pattern
	 */
	PatternBasedVersionFetcher(Pattern pattern) {
		this.pattern = pattern;
	}

	/* (non-Javadoc)
	 * @see com.chronopic.app.utils.useragent.VersionFetcher#version(java.lang.String)
	 */
	@Override
	public final Version version(String userAgentString) {
		Matcher matcher = pattern.matcher(userAgentString);
		if (!matcher.find()) {
			return null;
		}
		return createVersion(matcher);
	}
	
	/**
	 * Creates the version.
	 *
	 * @param matcher the matcher
	 * @return the version
	 */
	protected Version createVersion(Matcher matcher) {
		String fullVersionString = matcher.group(1);
		String majorVersion = matcher.group(2);
		String minorVersion = "0";
		if (matcher.groupCount() > 2) // usually but not always there is a minor version
			minorVersion = matcher.group(3);
		return new Version (fullVersionString,majorVersion,minorVersion);
	}

}

package cloud.kanda.platform.utils.network;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    /* URI get public ip */
    public final String URI_PUBLIC_IP = "https://checkip.amazonaws.com";

}

/*
 * 
 */
package cloud.kanda.platform.utils.useragent;

/**
 * Implementation of {@link VersionFetcher} that holds a list of other {@link VersionFetcher}
 * and calls them sequentially until any of them manages to find version.  
 * @author alexr
 */
public class SequentialVersionFetcher implements VersionFetcher {
	
	/** The fetchers. */
	private final VersionFetcher[] fetchers;
	
	/**
	 * Instantiates a new sequential version fetcher.
	 *
	 * @param first the first
	 * @param others the others
	 */
	public SequentialVersionFetcher(VersionFetcher first, VersionFetcher... others) {
		fetchers = new VersionFetcher[others.length + 1];
		fetchers[0] = first;
		for (int i = 0; i < others.length; i++) {
			fetchers[i+1] = others[i]; 
		}
	}

	/* (non-Javadoc)
	 * @see com.chronopic.app.utils.useragent.VersionFetcher#version(java.lang.String)
	 */
	@Override
	public Version version(String str) {
		for (VersionFetcher fetcher : fetchers) {
			Version version = fetcher.version(str);
			if (version != null) {
				return version;
			}
		}
		return null;
	}

}

package cloud.kanda.platform.utils.reflex;

import lombok.experimental.UtilityClass;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings(value = {"unchecked"})
@UtilityClass
public class UtilsReflex {

    public Collection<Field> getFieldsClass(final Class<?> clazz, final Boolean getSuperFields) {
        final Collection<Field> fields = new ArrayList<Field>(Arrays.asList(clazz.getDeclaredFields()));

        if (getSuperFields && isSuperClassObject(clazz)) {
            fields.addAll(getFieldsSuperClass(clazz));
        }

        return fields;
    }

    public Collection<Field> getFieldsSuperClass(final Class<?> clazz) {
        final Class<?> clazzSuper = clazz.getSuperclass();

        final Collection<Field> fields = new ArrayList<Field>(Arrays.asList(clazzSuper.getDeclaredFields()));

        if (isSuperClassObject(clazzSuper)) {
            fields.addAll(getFieldsSuperClass(clazzSuper));
        }

        return fields;
    }

    private Boolean isSuperClassObject(final Class<?> clazz) {
        return clazz.getSuperclass() != Object.class;
    }

    public Map<String, Field> parseListFieldsToMap(final Collection<Field> fields) {
        return fields.stream().distinct().collect(Collectors.toMap((field) -> {
                    ReflectionUtils.makeAccessible(field);
                    return field.getName();
                }, field -> field,
                // Si hay duplicados se queda con el primero guardado, suele ser el serialVersionUID
                (a, b) -> a));
    }

    public Class<?> getClassTyped(final Class<?> clazz, final int index) throws ClassNotFoundException {
        final Class<?> dtoClass = null;
        final Type mySuperclass = clazz.getGenericSuperclass();
        final Type tType = ((ParameterizedType) mySuperclass).getActualTypeArguments()[index];
        final String className = tType.toString().split(" ")[1];

        return Class.forName(className);
    }

    public <O> O getInstanceTypeClassReflection(final Class<?> clazzSuper, final int index) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException {
        final Class<O> clazz = (Class<O>) getClassTyped(clazzSuper, index);
        final List<Constructor<?>> constructors = Arrays.asList(clazz.getDeclaredConstructors()).stream().filter(c -> c.getParameterCount() == 0).collect(Collectors.toList());
        final Constructor<O> constructor = (Constructor<O>) constructors.get(0);

        return constructor.newInstance();
    }

}

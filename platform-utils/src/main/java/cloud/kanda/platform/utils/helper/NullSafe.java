package cloud.kanda.platform.utils.helper;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

@UtilityClass
public class NullSafe {

    public <T> T get(final Supplier<T> resolver) {
        return get(resolver, null);
    }

    public <T> T get(final Supplier<T> resolver, final T defaultValue) {
        return getPrivate(resolver).orElse(defaultValue);
    }

    private <T> Optional<T> getPrivate(final Supplier<T> resolver) {
        Optional<T> optional = Optional.empty();
        try {
            final T result = resolver.get();
            optional = Optional.ofNullable(result);
        } catch (final NullPointerException | IndexOutOfBoundsException e) {
            e.setStackTrace(new StackTraceElement[0]);
        }
        return optional;
    }

    public <T> Optional<T> getOptional(final Supplier<T> resolver) {
        return getPrivate(resolver);
    }

    public <T> Collection<T> getCollection(final Supplier<Collection<T>> resolver) {
        Optional<Collection<T>> optional = Optional.empty();
        try {
            final Collection<T> result = resolver.get();
            optional = Optional.ofNullable(result);
        } catch (final NullPointerException e) {
        }

        return optional.orElse(new ArrayList<T>());
    }

    public <T> Stream<T> getStream(final Supplier<T[]> resolver) {
        Stream<T> stream = Stream.empty();
        try {
            final T[] result = resolver.get();
            stream = Stream.of(result);
        } catch (final NullPointerException e) {
        }

        return stream;
    }

}

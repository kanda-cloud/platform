package cloud.kanda.platform.utils.network;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class NetworkUtilsTest {

    @Test
    public void getIp() {
        // Given

        // When
        final String result = NetworkUtils.getNATIp();

        // Assert
        Assertions.assertNotNull(result);
    }

    @Test
    public void getPublicIP() {
        // Given

        // When
        final String result = NetworkUtils.getPublicIP();

        // Assert
        Assertions.assertNotNull(result);
    }

}
